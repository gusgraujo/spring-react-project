# BeeWatching project


## Diagram
![Diagram](/uploads/79c03cb375b5c755449488ad86867384/Diagram.jpg)

## Setup API and database

**1-** Download [docker desktop](https://www.docker.com/products/docker-desktop/) on your computer. 

**2-** Clone the git repository to your computer.

**3-** Open a terminal in the root of your repository and make this sequence of commands:

* To free your docker container with you have the services already running.
> docker compose down
* To build the application and install the dependencies.

> docker compose build
* To set up the container that we build it.
> docker compose up

**4-** Enter the [swagger](http://localhost:8080/swagger-ui.html#/) link to see all the endpoints available.

**5-**  Enjoy :tada:

