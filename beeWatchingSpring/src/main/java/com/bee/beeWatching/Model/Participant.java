package com.bee.beeWatching.Model;

import com.bee.beeWatching.Model.Base.NamedBaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "participant")
public class Participant extends NamedBaseEntity {

    @Column(unique = true)
    private String discordName;

    @Lob
    private String avatarFileUpload;

    private Boolean statusDraw;


    @JsonIgnore
    @JsonManagedReference
    @ManyToMany(targetEntity = Movie.class, mappedBy = "participants")
    public List<Movie> movies;

    public String getDiscordName() {
        return discordName;
    }

    public void setDiscordName(String discordName) {
        this.discordName = discordName;
    }

    public String getAvatarFileUpload() {
        return avatarFileUpload;
    }

    public void setAvatarFileUpload(String avatarFileUpload) {
        this.avatarFileUpload = avatarFileUpload;
    }

    public void setStatusDraw(Boolean statusDraw) {
        this.statusDraw = statusDraw;
    }

    public Boolean isStatusDraw() {
        return this.statusDraw;
    }

    @Override
    public String toString() {
        return "Participant{" +
                "id=" + id +
                "name=" + name +
                ", discordName=" + discordName +
                ", avatarFileUpload=" + avatarFileUpload +
                ", movies=" + movies +
                '}';
    }
}
