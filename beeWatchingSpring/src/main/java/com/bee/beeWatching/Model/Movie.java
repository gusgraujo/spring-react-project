package com.bee.beeWatching.Model;


import com.bee.beeWatching.Model.Base.NamedBaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "movie")
@DynamicUpdate
@Getter
@Setter
public class Movie extends NamedBaseEntity {

    private String idMovie;


    @ApiModelProperty(readOnly = true)
    private Integer year;


    private String background;

    @ApiModelProperty(readOnly = true)
    private String imageURL;


    @ApiModelProperty(readOnly = true)
    private Double duration;

    @ApiModelProperty(readOnly = true)
    private Double rateIMDB;

    @ApiModelProperty(readOnly = true)
    private Double rateMeta;

    @ApiModelProperty(readOnly = true)
    @Lob
    private String trailerUrl;


    private Boolean status;

    @ElementCollection
    @ApiModelProperty(accessMode = ApiModelProperty.AccessMode.READ_ONLY)
    private List<String> categoryList;

    @Transient
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Integer participantId;


    @JsonIgnore
    @JsonBackReference
    @ManyToMany
    @JoinTable(
            name = "season_movie",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "season_id"))
    @ApiModelProperty(readOnly = true)
    private List<Season> seasons;

    @JsonIgnore
    @JsonBackReference
    @ManyToMany
    @JoinTable(
            name = "participant_movie",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "participant_id"))
    @ApiModelProperty(readOnly = true)
    private List<Participant> participants;

    public Movie() {
    }

    public Movie(String idMovie, String name, Integer year, String background ,String imageURL,
                 String trailerUrl, Double duration, Double rateIMDB, Double rateMeta,
                 List<Participant> participants,List<Season> seasons, Boolean status) {
        this.idMovie = idMovie;
        this.name = name;
        this.year = year;
        this.background = background;
        this.imageURL = imageURL;
        this.trailerUrl = trailerUrl;
        this.duration = duration;
        this.rateIMDB = rateIMDB;
        this.rateMeta = rateMeta;
        this.seasons = seasons;
        this.status = status;
        this.participants = participants;
    }
    public void setParticipantId(Integer participantId){
        this.participantId = participantId;
    }


    public Integer getParticipantId(){
        return this.participantId;
    }


    @Override
    public String toString() {
        return "Movie{" +
                "id='" + id + '\'' +
                "idMovie='" + idMovie + '\'' +
                ", year=" + year +
                ", background='" + background + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", duration=" + duration +
                ", rateIMDB=" + rateIMDB +
                ", rateMeta=" + rateMeta +
                ", trailerUrl='" + trailerUrl + '\'' +
                ", status=" + status +
                ", seasons=" + seasons +
                '}';
    }
}
