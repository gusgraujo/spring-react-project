package com.bee.beeWatching.Model;

import com.bee.beeWatching.Model.Base.NamedBaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Table(name = "award")
@DynamicUpdate
public class Award extends NamedBaseEntity {

    @NotNull
    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
    @JoinColumn(name="idSeason", nullable = false)
    private Season season = new Season();


    @Transient
    private Movie movie = new Movie();

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public String idImdb;

    public Award() {
    }

    public Award(String name, Season season, Movie movie,String idImdb) {

        this.name = name;
        this.season = season;
        this.movie = movie;
        this.idImdb = idImdb;
    }


    public void setSeasonId(Integer idSeason){
        this.season.setId(idSeason);
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public Integer getSeasonId(){
        return this.season.getId();
    }

    @ApiModelProperty(hidden = true)
    public void setMovie(Movie movie){
        this.movie = movie;
    }

    @ApiModelProperty(hidden = true)
    public Movie getMovie(){
        return this.movie;
    }


    public void setIdImdb(String idImdb) {
        this.idImdb = idImdb;
    }

    @JsonIgnore
    public String getIdImdb() {
        return this.idImdb;
    }

    @ApiModelProperty(hidden = true)
    public Season getSeason() {
        return this.season;
    }

    @ApiModelProperty(hidden = true)
    public void setSeason(Season season) {
        this.season = season;
    }

}