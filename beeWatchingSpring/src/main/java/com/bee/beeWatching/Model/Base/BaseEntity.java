package com.bee.beeWatching.Model.Base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
public class BaseEntity {

    @Id
    @ApiModelProperty(hidden = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public int id;

    @NotNull
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    @Column(name = "createdAt")
    public Date createdAt;

    @NotNull
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    @Column(name = "updatedAt")
    public Date updatedAt;

    @NotNull
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    @Column(name = "guuid")
    public UUID guuid;

    @NotNull
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    @Transient
    public Integer errorCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public UUID getGuuid() {
        return guuid;
    }

    public void setGuuid(UUID guuid) {
        this.guuid = guuid;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public Integer getErrorCode() {
        return errorCode;
    }
}
