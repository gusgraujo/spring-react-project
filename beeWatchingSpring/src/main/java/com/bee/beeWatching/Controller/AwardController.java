package com.bee.beeWatching.Controller;

import com.bee.beeWatching.Model.Award;
import com.bee.beeWatching.Repository.MovieRepository;
import com.bee.beeWatching.Service.AwardService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/award")
@CrossOrigin
public class AwardController {

    @Autowired
    private AwardService awardService;

    private static final Logger logger = LogManager.getLogger(ParticipantController.class);
    @Autowired
    private MovieRepository movieRepository;

    @GetMapping("/get")
    public ResponseEntity<List<Award>> getAwards(@RequestParam(defaultValue = "0") Integer idSeason,
                                                 @RequestParam(defaultValue = "") String name,
                                                 @RequestParam(defaultValue = "") String idImdb) throws IOException{
        try{
            List<Award> awards;
            if(idSeason != 0){
                logger.info("getAwardsBySeason "+idSeason);
                awards = awardService.getAwardsBySeason(idSeason);
            }

            else if(!name.equals("")){
                logger.info("getAwardsByName "+name);
                awards = awardService.getAwardsByName(name);
            }

            else if(!idImdb.equals("")){
                logger.info("getAwardsByImdb "+idImdb);
                awards = awardService.getAwardsByImdb(idImdb);
            }

            else{
                logger.info("getAllAwards");
                awards = awardService.getAllAwards();
            }


            awards.forEach(award -> award.setMovie(movieRepository.getMovieByImdb(award.getIdImdb())));

            return new ResponseEntity<>(awards, HttpStatus.CREATED);
        }
        catch (Exception e){
            logger.error("Error occured while getting Awards", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Award> createAward(@RequestBody @Validated Award award) throws IOException{
        try{
            Award savedAward = awardService.createAward(award);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e){
            logger.error("Error occured while creating Award", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteAwardById(@PathVariable Integer id) {
        try {
            awardService.deleteAwardById(id);
            logger.info("Award with id {} was successfully deleted.", id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            logger.error("An error occurred while deleting award with id {}", id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Award> updateAward(@PathVariable Integer id, @Validated @RequestBody Award award)
    {
        try {
            Award updatedAward = awardService.updateAward(id, award);
            if (updatedAward != null) {
                logger.info("Award with id {} updated successfully", id);
                return new ResponseEntity<>(updatedAward, HttpStatus.OK);
            } else {
                logger.error("Award with id {} not found", id);
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            logger.error("Error while updating award with id {}: {}", id, e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
