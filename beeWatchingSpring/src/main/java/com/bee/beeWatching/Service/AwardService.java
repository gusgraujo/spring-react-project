package com.bee.beeWatching.Service;

import com.bee.beeWatching.Exception.ResourceNotFoundException;
import com.bee.beeWatching.Model.Award;

import java.util.List;

public interface AwardService {
    List<Award> getAwardsBySeason(Integer idSeason);

    List<Award> getAwardsByName(String name);

    List<Award> getAwardsByImdb(String idImdb);

    List<Award> getAllAwards();

    Award createAward(Award award);

    void deleteAwardById(Integer id);

    Award updateAward(Integer id, Award award) throws ResourceNotFoundException;;
}
