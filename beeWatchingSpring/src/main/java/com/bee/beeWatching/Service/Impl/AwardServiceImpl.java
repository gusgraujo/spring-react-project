package com.bee.beeWatching.Service.Impl;

import com.bee.beeWatching.Exception.ResourceNotFoundException;
import com.bee.beeWatching.Model.Award;
import com.bee.beeWatching.Model.Participant;
import com.bee.beeWatching.Model.Season;
import com.bee.beeWatching.Repository.AwardRepository;
import com.bee.beeWatching.Exception.ResourceNotFoundException;
import com.bee.beeWatching.Repository.Base.GenericRepository;
import com.bee.beeWatching.Service.AwardService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AwardServiceImpl implements AwardService {

    @Autowired
    AwardRepository awardRepository;

    private static final Logger logger = LogManager.getLogger(ParticipantServiceImpl.class);
    @Autowired
    @Qualifier("seasonRepository")
    private GenericRepository seasonRepository;

    public List<Award> getAwardsBySeason(Integer idSeason){

        return awardRepository.getAwardsBySeason(idSeason);
    }

    public List<Award> getAwardsByName(String name){

        return awardRepository.getAwardsByName(name);
    }

    public List<Award> getAwardsByImdb(String imdb){

        return awardRepository.getAwardsByImdb(imdb);
    }

    public List<Award> getAllAwards(){

        return awardRepository.findAll();
    }

    public Award createAward(Award award) throws RuntimeException{
        try {
            if (award.getGuuid() == null) {
                award.setGuuid(UUID.randomUUID());
            }
            if (award.getCreatedAt() == null) {
                award.setCreatedAt(new Date());
            }
            logger.info("award name: "+award.getName());
            return awardRepository.save(award);
        } catch (Exception e) {
            logger.error("Error saving award: " + e.getMessage(), e);
            throw new RuntimeException("Error saving award", e);
        }
    }

    public void deleteAwardById(Integer id){ awardRepository.deleteById(id);}


    public Award updateAward(Integer id, Award award) {
        try{
            boolean updated = false;
            Award existingAward = awardRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Award not found with id: " + id));

            if(!award.getIdImdb().isBlank()){
                existingAward.setIdImdb(award.getIdImdb());
                updated = true;
            }
            if(!award.getName().isBlank()){
                existingAward.setName(award.getName());
                updated = true;
            }
            if(award.getSeasonId() > 0 && !award.getSeasonId().equals(existingAward.getSeasonId())){
                Optional<Season> season = seasonRepository.findById(award.getSeasonId());
                season.ifPresent(existingAward::setSeason);
                updated = true;
            }
            if(updated)
                existingAward.setUpdatedAt(new Date());

            return createAward(existingAward);
        }
        catch (ResourceNotFoundException e){
            logger.error("Award not found with id: " + id);
            return null;
        }

    }
}
