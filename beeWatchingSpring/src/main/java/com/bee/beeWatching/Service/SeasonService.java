package com.bee.beeWatching.Service;

import com.bee.beeWatching.Exception.ResourceNotFoundException;
import com.bee.beeWatching.Model.Season;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface SeasonService{

    public Season getCurrentSeason();

    public boolean isBetweenSeason(Date dateStart, Date dateEnd);

    public Season updateSeason(int id, Season season) throws ResourceNotFoundException;

    public Page<Season> getSeasonsByName(String name, Pageable season);
    Season saveSeason(Season entity);

    Season findById(int id);

    Page<Season> findAll(Pageable seasonPage);

    void deleteById(int id);
}
