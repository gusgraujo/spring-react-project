package com.bee.beeWatching.Service.Impl;

import com.bee.beeWatching.Controller.MovieController;
import com.bee.beeWatching.Exception.ResourceNotFoundException;
import com.bee.beeWatching.Model.Movie;
import com.bee.beeWatching.Model.Participant;
import com.bee.beeWatching.Repository.Base.GenericRepository;
import com.bee.beeWatching.Repository.MovieRepository;
import com.bee.beeWatching.Service.MovieService;
import com.bee.beeWatching.Service.ParticipantService;
import com.bee.beeWatching.Utils.RequestApiMovie;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;


@Service
public class MovieServiceImpl implements MovieService {
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(MovieController.class);
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    ParticipantService participantService;

    RequestApiMovie requestUtil;


    public MovieServiceImpl() {
        requestUtil = new RequestApiMovie();
    }

    @Override
    public Movie getMovieById(String idMovie) throws IOException {
            return requestUtil.formatMovie(idMovie);
    }
    @Override
    public String getMovieTrailerById(String idMovie) throws IOException {
        logger.info("getMovieTrailerById -> "+idMovie);
        return requestUtil.findMovieTrailerById(idMovie);
    }
    @Override
    public List<Movie> getMoviesBySeason(int idSeason) throws IOException{
        logger.info("MovieServiceImpl::getMoviesBySeason INI");
        List<Movie> movies = movieRepository.getMoviesBySeason(idSeason);
        logger.info("MovieServiceImpl::getMoviesBySeason END");
        return movies;
    }
    @Override
    public Movie getMovieByTitle(String title) throws IOException {
        return requestUtil.formatMovie(requestUtil.findMovieByTitle(title)) ;
    }

    @Override
    public Movie save(Movie entity)
    {
        try {
            if (entity.getGuuid() == null) {
                entity.setGuuid(UUID.randomUUID());
            }
            if (entity.getCreatedAt() == null) {
                entity.setCreatedAt(new Date());
            }
            entity.setUpdatedAt(new Date());
            return movieRepository.save(entity);
        } catch (Exception e) {
            logger.error("Error saving movie: " + e.getMessage(), e);
            throw new RuntimeException("Error saving movie", e);
        }

    }

    @Override
    public Movie findById(int id) {
        Optional<Movie> optionalEntity = movieRepository.findById(id);
        return optionalEntity.orElse(null);
    }

    @Override
    public List<Movie> findAll() {
        logger.info("MovieServiceImpl::findAll INI");
        List<Movie> movieList = movieRepository.findAll();
        logger.info("MovieServiceImpl::findAll END");
        return movieList;
    }


    @Override
    public Movie findCurrentMovie() {
        logger.info("MovieServiceImpl::findCurrentMovie INI");
        Movie currentMovie = movieRepository.findCurrentMovie();
        logger.info("MovieServiceImpl::findCurrentMovie END");
        return currentMovie;
    }

    @Override
    public void deleteById(int id) throws RuntimeException{
        try{
            logger.info("MovieServiceImpl::deleteById INI");
            movieRepository.deleteById(id);
            logger.info("MovieServiceImpl::deleteById END");
        }
        catch (RuntimeException e){
            logger.error("Error deleting movie");
        }
    }

    @Override
    public Movie updateMovie(int id, Movie movie) throws ResourceNotFoundException {
        try{
            logger.info("MovieServiceImpl::updateMovie INI");
            Movie existingMovie = movieRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Movie not found with id: " + id));

            logger.info("Check if update all movie fields");
            if(movie.getIdMovie() != null && !movie.getIdMovie().equals(existingMovie.getIdMovie())){
                Movie newIdMovie = getMovieById(movie.getIdMovie());
                existingMovie.setIdMovie(newIdMovie.getIdMovie());
                existingMovie.setYear(newIdMovie.getYear());
                existingMovie.setRateMeta(newIdMovie.getRateMeta());
                existingMovie.setRateIMDB(newIdMovie.getRateIMDB());
                existingMovie.setDuration(newIdMovie.getDuration());
                existingMovie.setBackground(newIdMovie.getBackground());
                existingMovie.setTrailerUrl(getMovieTrailerById(newIdMovie.getIdMovie()));
                existingMovie.setName(newIdMovie.getName());
                existingMovie.setImageURL(newIdMovie.getImageURL());
                existingMovie.setCategoryList(movie.getCategoryList());
                logger.info("Updated all movie fields");
            }

            logger.info("Check if update participant");
            if(movie.getParticipantId() != null && movie.getParticipantId() > 0){
                List<Participant> newParticipantList = new ArrayList<>();
                Optional<Participant> newParticipant = Optional.ofNullable(participantService.findById(movie.getParticipantId()));
                if(newParticipant.isPresent() && newParticipant.get().getId() > 0){
                    newParticipantList.add(newParticipant.get());
                    existingMovie.setParticipants(newParticipantList);
                    logger.info("Updated movie participant");
                }
            }

            logger.info("Check if update status");
            if(movie.getStatus() != null && movie.getStatus() != existingMovie.getStatus()){
                existingMovie.setStatus(movie.getStatus());
                logger.info("Updated status");
            }


            logger.info("Check if update background");
            if(movie.getBackground() != null && !movie.getBackground().equals(existingMovie.getBackground())){
                existingMovie.setBackground(movie.getBackground());
                logger.info("Updated background");
            }

            logger.info("MovieServiceImpl::updateMovie END");
            return save(existingMovie);
        }catch (ResourceNotFoundException e){
            logger.error("Error updating Movie with id: " + id);
            return null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
