package com.bee.beeWatching.Repository;

import com.bee.beeWatching.Model.Movie;
import com.bee.beeWatching.Repository.Base.GenericRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
@Qualifier("movieRepository")
public interface MovieRepository extends GenericRepository<Movie, Integer> {

    @Query(value = "SELECT * FROM movie WHERE status is true; ",nativeQuery = true)
    Movie findCurrentMovie();

    @Query(value = "SELECT m.*  FROM movie m, season_movie sm WHERE sm.season_id = :idSeason AND sm.movie_id = m.id; ",nativeQuery = true)
    List<Movie> getMoviesBySeason(Integer idSeason);

    @Query(value = "SELECT * from movie m where id_movie like :idImdb limit 1",nativeQuery = true)
    Movie getMovieByImdb(@Param("idImdb")String idImdb);


}
