package com.bee.beeWatching.Repository;

import com.bee.beeWatching.Model.Movie;
import com.bee.beeWatching.Model.Participant;
import com.bee.beeWatching.Repository.Base.GenericRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ParticipantRepository extends GenericRepository<Participant, Integer> {
    @Modifying
    @Query(value = "delete from participant_movie where participant_id = :participantId",nativeQuery = true)
    public void deleteParticipantMovieById(int participantId);
}
