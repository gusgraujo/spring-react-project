package com.bee.beeWatching.Repository;

import com.bee.beeWatching.Model.Award;
import com.bee.beeWatching.Model.Movie;
import com.bee.beeWatching.Repository.Base.GenericRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AwardRepository extends GenericRepository<Award, Integer> {

    @Query(value = "select distinct a.* from ((award a inner join season_movie sm on a.id_season  = sm.season_id )\n" +
            " inner join movie m on sm.movie_id = m.id) where a.id_season = :idSeason ",nativeQuery = true)
    public List<Award> getAwardsBySeason(Integer idSeason);

    @Query(value = "select distinct a.* from ((award a inner join season_movie sm on a.id_season  = sm.season_id )\n" +
            " inner join movie m on sm.movie_id = m.id) where a.name = :name ",nativeQuery = true)
    public List<Award> getAwardsByName(String name);

    @Query(value = "select distinct a.* from ((award a inner join season_movie sm on a.id_season  = sm.season_id )\n" +
            " inner join movie m on sm.movie_id = m.id) where a.id_imdb = :idImdb ",nativeQuery = true)
    public List<Award> getAwardsByImdb(String idImdb);



}
