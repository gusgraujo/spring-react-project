import { _Checkbox } from "./_CheckBox";

const MasterCheckBox = (props: any) => {
  const { valueId, allChecked, setAllChecked } = props;

  return (
    <_Checkbox>
      <input
        type="checkbox"
        value={valueId}
        onChange={() => setAllChecked(!allChecked)}
      />
      <span className="checkmark"></span>
    </_Checkbox>
  );
};
export default MasterCheckBox;
