import styled from "styled-components";

export const _Checkbox = styled.section`
  input[type="checkbox"] {
    appearance: none;
    background-color: #939393;
    margin: 0;
    font: inherit;
    color: #000000;
    width: 1.15em;
    height: 1.15em;
    border: 0.15em solid #5c5c5c;
    border-radius: 0.15em;
    transform: translateY(-0.075em);
  }

  input[type="checkbox"]:checked {
    appearance: auto;
    accent-color: #718096;
    color: #000000;
  }
`;
