import { useEffect } from "react";
import { _Checkbox } from "./_CheckBox";

const CheckBox = (props: any) => {
  const { item, handleCheck } = props;

  //const [selfChecked, setSelfChecked] = useState<boolean>(allChecked);

  useEffect(() => {
    // setSelfChecked(allChecked);
  }, []);

  return (
    <_Checkbox>
      <input
        className="ck"
        type="checkbox"
        value={item}
        onChange={handleCheck}
      />
    </_Checkbox>
  );
};
export default CheckBox;
