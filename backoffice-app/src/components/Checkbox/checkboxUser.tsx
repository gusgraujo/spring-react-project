import { useEffect } from "react";
import { _Checkbox } from "./_CheckBox";
import checkbox from "../Checkbox/interfaceCheckBox"

const CheckBox = (props: checkbox) => {
    const { item, handleCheck, selfChecked,handleAll } = props;


    //const [selfChecked, setSelfChecked] = useState<boolean>(allChecked);

    useEffect(() => {
        // setSelfChecked(allChecked);
    }, []);

    return (

        <>

            {selfChecked ?
                <_Checkbox>
                    <input
                        className="ck"
                        type="checkbox"
                        value={item}
                        onChange={handleAll}
                        checked={selfChecked} 
                        />
                </_Checkbox>
                :
                <_Checkbox>
                    <input
                        className="ck"
                        type="checkbox"
                        value={item}
                        onChange={handleCheck} 
                        />
                </_Checkbox>}
        </>
    )
};
export default CheckBox;