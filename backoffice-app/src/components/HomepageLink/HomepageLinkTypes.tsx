export interface HomepageLinkTypes {
  backgroundImage: string;
  text: string;
  icon: any;
  route: string;
}
