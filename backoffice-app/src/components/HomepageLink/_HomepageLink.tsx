import styled from "styled-components";

export const _HomepageLink = styled.section`
  margin: 2rem;

  & > a {
    display: grid;
    grid-template-columns: 0fr 3fr;
    height: 10rem;
    color: white;
    width: 30rem;
    justify-content: center;
    align-items: center;
  }

  img {
    transform: scale(1.4);
  }

  .background {
    height: 7rem;
    width: 25rem;
    position: absolute;
    z-index: -1;
  }

  .icon {
    font-size: 75px;
    justify-self: center;
  }

  .textArea {
    font-size: 20px;
    text-align: center;
  }
`;
