import { Link } from "react-router-dom";
import { HomepageLinkTypes } from "./HomepageLinkTypes";
import { _HomepageLink } from "./_HomepageLink";

const HomepageLink = ({
  backgroundImage,
  text,
  icon,
  route,
}: HomepageLinkTypes) => {
  return (
    <_HomepageLink>
      <Link to={route}>
        <figure className="background">
          <img src={backgroundImage} alt="boxHomeBackground" />
        </figure>
        <section className="icon">{icon}</section>
        <section className="textArea">
          <article>Create</article>
          <article>{text}</article>
        </section>
      </Link>
    </_HomepageLink>
  );
};

export default HomepageLink;
