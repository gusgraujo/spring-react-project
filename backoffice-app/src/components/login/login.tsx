import axios from "axios";
import { useState } from "react";
import { FaLock, FaUserAlt, FaUserCircle } from "react-icons/fa";
import { IoWarning } from "react-icons/io5";
import styled from "styled-components";
import { StyledLogin } from "./StyledLogin";

const Container = styled.div`
  margin: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: red;
`;

const Login = (props: any) => {
  const [userData, setUserData] = useState({
    username: "",
    password: "",
  });
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>("");

  const login = (body: Object) => {
    return axios
      .post("http://localhost:8080/users/login", body)
      .then(({ status }) => {
        if (status === 200) props.setIsLoggedIn(true);
      })
      .catch(({ response: { data } }) => {
        setErrorMessage(data);
      });
  };

  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const { name, value } = e.target;

    setUserData((values) => ({
      ...values,
      [name]: value,
    }));
  }

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (!userData.username || !userData.password) return;

    login(userData);
  }

  return (
    <StyledLogin>
      <section className="textAndIconContainer">
        <FaUserCircle color="#319695" size={50} />
        <p>Welcome</p>
      </section>

      <form className="loginContainer" onSubmit={handleSubmit}>
        <label className="label">
          <FaUserAlt color="#cbd5e0" className="labelIcon" />
          <input
            required
            onChange={handleChange}
            type="text"
            id="username"
            name="username"
            placeholder="username"
            className="loginContainerInput"
          />
        </label>

        <label className="label">
          <FaLock color="#cbd5e0" className="labelIcon" />
          <input
            required
            onChange={handleChange}
            type={showPassword ? "text" : "password"}
            id="password"
            name="password"
            placeholder="password"
            className="loginContainerInput"
          />
          <button
            onClick={() => setShowPassword(!showPassword)}
            type="button"
            className="showButton"
          >
            Show
          </button>
        </label>

        {errorMessage && (
          <Container>
            <IoWarning color="red" size={24} />
            <label>{errorMessage}</label>
          </Container>
        )}

        <button type="submit" className="loginButton">
          Login
        </button>
      </form>
    </StyledLogin>
  );
};

export default Login;
