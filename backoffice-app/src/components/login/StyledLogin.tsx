import styled from "styled-components";

export const StyledLogin = styled.section`
  background-color: #e2e8f0;
  height: 100vh;
  width: 100vw;
  display: grid;
  grid-template-rows: 1fr 2fr;
  align-items: center;
  justify-content: center;

  .textAndIconContainer {
    display: flex;
    flex-flow: column;
    align-items: center;
    color: #38b3ac;
    font-family: "CanaroSemibold";
    font-size: 2.5rem;
    align-self: end;
  }

  .loginContainer {
    width: 30rem;
    background-color: white;
    display: flex;
    flex-flow: column;
    justify-content: center;
    padding: 10px;
    box-shadow: 1px 9px 11px 4px #cbd3df;
    align-self: baseline;
  }

  .label {
    position: relative;
    display: block;
  }

  .labelIcon {
    pointer-events: none;
    position: absolute;
    transform: translate(20px, 20px);
  }

  .loginContainerInput {
    padding-left: 1.75rem;
    padding-top: 0.25rem;
    padding-bottom: 0.25rem;
    border-width: 1px;
    border-radius: 0.25rem;
    margin: 0.75rem;
    width: 94%;
  }

  .loginButton {
    font-weight: 600;
    cursor: pointer;
    margin: 0.75rem;
    color: white;
    background-color: #319695;
    width: 94%;
    height: 2.5rem;
  }

  .showButton {
    font-weight: 600;
    padding-left: 0.75rem;
    padding-right: 0.75rem;
    background-color: #edf3f7;
    border-radius: 0.25rem;
    cursor: pointer;
    position: absolute;
    transform: translate(-89px, 17px);
  }
`;
