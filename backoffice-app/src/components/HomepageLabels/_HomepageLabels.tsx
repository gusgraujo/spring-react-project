import styled from "styled-components";

export const _HomepageLabels = styled.section`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  width: 99vw;
  height: 87vh;
  justify-content: center;
  align-items: center;

  & > section:first-child {
    display: flex;
    justify-content: end;
    align-self: end;
  }

  & > section:last-child {
    display: flex;
    justify-content: start;
    align-self: start;
  }

  & > section:nth-child(2) {
    display: flex;
    justify-content: start;
    align-self: end;
  }

  & > section:nth-child(3) {
    display: flex;
    justify-content: end;
    align-self: start;
  }
`;
