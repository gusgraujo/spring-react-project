import { BsCameraReelsFill } from "react-icons/bs";
import HomepageLink from "../HomepageLink/HomepageLink";
import { _HomepageLabels } from "./_HomepageLabels";
import label from "../../assets/images/box_home.png";
import { ImTrophy } from "react-icons/im";
import { FaCalendarAlt, FaUsers } from "react-icons/fa";

const HomepageLabels = () => {
  return (
    <_HomepageLabels>
      <HomepageLink
        backgroundImage={label}
        text={"Movie"}
        icon={<BsCameraReelsFill />}
        route={"/Movie"}
      />
      <HomepageLink
        backgroundImage={label}
        text={"Seasons"}
        icon={<FaCalendarAlt />}
        route={"/Seasons"}
      />
      <HomepageLink
        backgroundImage={label}
        text={"Users"}
        icon={<FaUsers />}
        route={"/Users"}
      />
      <HomepageLink
        backgroundImage={label}
        text={"Awards"}
        icon={<ImTrophy />}
        route={"/Awards"}
      />
    </_HomepageLabels>
  );
};

export default HomepageLabels;
