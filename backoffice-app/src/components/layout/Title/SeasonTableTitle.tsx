import { useState } from "react";
import {
  AiOutlineSortAscending,
  AiOutlineSortDescending,
} from "react-icons/ai";
import MasterCheckBox from "../../Checkbox/MasterCheckBox";
import { _SeasonTitle } from "./_SeasonTitle";
const SeasonTableTitle = (props: any) => {
  const { name, actions, allChecked, setAllChecked, sort, toggleSort } = props;


  return (
    <_SeasonTitle>
      <section>
        <MasterCheckBox allChecked={allChecked} setAllChecked={setAllChecked} />
      </section>
      <section>
        <button className="sortButton" onClick={() => toggleSort()}>
          <article>{name}</article>
          {sort ? (
            <AiOutlineSortDescending size={20} />
          ) : (
            <AiOutlineSortAscending size={20} />
          )}
        </button>
      </section>
      <section>
        <article>{actions}</article>
      </section>
    </_SeasonTitle>
  );
};
export default SeasonTableTitle;
