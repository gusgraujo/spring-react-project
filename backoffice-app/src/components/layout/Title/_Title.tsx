import styled from "styled-components";

export const _Title = styled.section`
  color: #434343;
  font-weight: 600;
  font-size: 2.25rem;
  line-height: 2.5rem;
  height: 2.5rem;
`;
