export interface UserTableTitleInterface {
  image: string;
  name: string;
  discordName: string;
  actions: string;
  allChecked: boolean;
  setAllChecked: Function;
  deleteAll: Function;
  show: boolean;
  item?: number;
  handleCheck?: Function;
  toggleAllSelected: Function;
}
