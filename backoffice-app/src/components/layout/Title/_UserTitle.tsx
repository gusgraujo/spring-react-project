import styled from "styled-components";

export const _UserTitle = styled.section`
  display: grid;
  grid-template-columns: 1fr 1fr 2fr 4fr 1fr 3fr;
  align-items: end;
  font-family: "CanaroSemibold";
  font-size: 17px;
  border-bottom: 2px solid black;

  & > section {
    padding-top: 1rem;
    padding-bottom: 1rem;
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    justify-self: center;
  }

  & > section:first-child {
    justify-self: start;
  }

  & > section:last-child {
    justify-self: end;
  }

  .sortButton,
  .masterCheckboxContainer {
    display: flex;
  }

  .masterCheckboxContainer {
    align-items: end;
  }

  .searchContainer {
    margin: 0px 800px;
    display: grid;
    grid-template-columns: 1fr 4fr 1fr;
    align-items: baseline;
  }
`;
