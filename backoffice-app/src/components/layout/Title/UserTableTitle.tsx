import { useState } from "react";
import {
  AiOutlineEye,
  AiOutlineSortAscending,
  AiOutlineSortDescending,
} from "react-icons/ai";
import MasterCheckBox from "../../Checkbox/MasterCheckBox";
import { DeleteMultiple } from "../Table/DeleteAll";
import { UserTableTitleInterface } from "./UserTableTitleInterface";
import { _UserTitle } from "./_UserTitle";

const UserTableTitle = ({
  image,
  name,
  discordName,
  actions,
  allChecked,
  setAllChecked,
  deleteAll,
  show,
  toggleAllSelected,
}: UserTableTitleInterface) => {
  const [sort, setSort] = useState<Boolean>(true);

  const ToggleSort = () => {
    setSort(!sort);
  };

  return (
    <_UserTitle>
      <section className="masterCheckboxContainer">
        <MasterCheckBox allChecked={allChecked} setAllChecked={setAllChecked} />
        <DeleteMultiple deleteMultiple={() => deleteAll()} show={show} />
        <AiOutlineEye onClick={() => toggleAllSelected()} />
      </section>
      <section>{image}</section>
      <section>
        <button className="sortButton" onClick={() => ToggleSort()}>
          {name}
          {sort ? (
            <AiOutlineSortDescending size={20} />
          ) : (
            <AiOutlineSortAscending size={20} />
          )}
        </button>
      </section>
      <section>{discordName}</section>
      <section>Active</section>
      <section>{actions}</section>
    </_UserTitle>
  );
};
export default UserTableTitle;
