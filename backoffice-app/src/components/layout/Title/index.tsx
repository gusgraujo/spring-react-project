import { _Title } from "./_Title";

const Title = (props: any) => {
  return <_Title>{props.title}</_Title>;
};
export default Title;
