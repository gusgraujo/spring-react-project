import "../../../App.css";

const MasterCheckBox = (props: any) => {
  const { valueId, allChecked, setAllChecked } = props;

  return (
    <input
      className="ck"
      type="checkbox"
      value={valueId}
      onChange={() => setAllChecked(!allChecked)}
    />
  );
};
export default MasterCheckBox;
