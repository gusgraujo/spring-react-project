import "../../../App.css";
import { BsTrash } from "react-icons/bs";
import { _DeleteAll } from "./_DeleteAll";
import DeleteAll from "./interfaceDeleteAll";

export const DeleteMultiple = (props:DeleteAll) => {
  const {deleteMultiple,show}= props
  return (
    <>
    <_DeleteAll>
      {show === true?<button className="delete-all"
       onClick={function(){deleteMultiple()}}>
        <BsTrash color="red" size={25} />
      </button>:null}
      </_DeleteAll>
    </>
  );
};

