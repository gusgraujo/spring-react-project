import { FaRegEdit } from "react-icons/fa";
import { BsTrash } from "react-icons/bs";
const Actions = (props: any) => {
  const { handleEdit, handleDelete, value, name, allUserInfo } = props;
  return (
    <>
      {handleEdit ? (
        <button onClick={() => handleEdit(value, allUserInfo)}>
          <FaRegEdit color="#1564f1" size={35} />
        </button>
      ) : null}
      {handleDelete ? (
        <button onClick={() => handleDelete(value, name)}>
          <BsTrash color="#f90101" size={35} />
        </button>
      ) : null}
    </>
  );
};
export default Actions;
