import styled from "styled-components";

export const StyledTable = styled.section`
  display: flex;
  align-items: center;
  flex-direction: column;

  .pageHeader {
    width: 100%;
    display: grid;
    grid-template-columns: 1.3fr 8fr 1fr;
    flex-direction: row;
    justify-items: end;
    margin-bottom: 10px;
    height: 5rem;
    align-items: end;
  }

  .buttonContainer {
    display: flex;
    align-self: end;
  }

  .addNewButton {
    height: 2rem;
    width: 6rem;
    font-family: "CanaroBook";
    font-size: 15px;
    border: 1px solid black;
    border-radius: 25px;
    background-color: #57b66f;
    border: 1px solid black;
    box-shadow: 5px 5px 5px #858282;
    font-weight: bold;
    align-self: flex-end;
    -webkit-text-stroke: thin;
  }

  .userTable {
    width: 80%;
    font-family: "CanaroSemibold";
  }

  .userTableContainer {
    width: 100%;
  }

  .userTableRow {
    height: 4rem;
    display: grid;
    grid-template-columns: 1fr 1fr 2fr 4fr 1fr 3fr;
    align-items: center;
  }

  .userTableRow > section {
    padding-top: 1rem;
    padding-bottom: 0.5rem;
    padding-left: 1.5rem;
    padding-right: 1.5rem;
  }

  .userTableRow > section > article {
    font-size: 15px;
  }

  .userTableRow > section,
  .userTableRow > figure {
    justify-self: center;
  }

  .userTableRow > section:last-child {
    justify-self: flex-end;
  }

  .userTableRow > section:first-child {
    justify-self: flex-start;
  }

  .userList > .userTableRow:nth-child(odd) {
    background: linear-gradient(
      90deg,
      rgba(147, 147, 147, 1) 0%,
      rgba(194, 194, 194, 1) 35%,
      rgba(147, 147, 147, 1) 100%
    );
  }

  .userTableRow > section > button:first-child {
    margin-right: 10px;
  }

  .searchIcon {
    position: absolute;
    width: 2.25rem;
    margin-right: 0.25rem;
  }

  .search {
    color: #000;
    background-color: #afafaf;
    margin-left: 5px;
    font-family: "CanaroSemiBold";
    font-size: 13px;
    display: flex;
    align-self: flex-end;
  }

  .search::placeholder {
    color: #797f87;
  }

  .search:focus {
    outline: none;
  }

  .searchAddNewContainer {
    display: flex;
  }

  .searchContainer {
    display: flex;
    align-self: flex-end;
    justify-content: space-around;
    align-items: center;
    padding-left: 0.75rem;
    padding-right: 0.75rem;
    padding-top: 0.25rem;
    padding-bottom: 0.25rem;
    background-color: #afafaf;
    border: 1px solid #626161;
    border-radius: 25px;
    margin-right: 0.75rem;
    height: 2rem;
    box-shadow: 5px 5px 5px #858282;
  }

  .searchContainer > svg {
    color: #797f87;
  }
`;
