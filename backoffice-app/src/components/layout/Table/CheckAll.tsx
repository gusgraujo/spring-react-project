import '../../../App.css'
const CheckAll  = (props:any) => {
    const { item,handleCheck} = props;
    return(
        <>
        <input
        className="ck"
        type="checkbox"
        value={item}
        onChange={handleCheck}/>
        </>
    )
}
export default CheckAll