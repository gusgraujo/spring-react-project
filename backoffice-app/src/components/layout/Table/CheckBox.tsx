import { useEffect, useState } from "react";
import "../../../App.css";

const CheckBox = (props: any) => {
  const { valueId, allChecked } = props;

  const [selfChecked, setSelfChecked] = useState<boolean>(allChecked);

  useEffect(() => {
    setSelfChecked(allChecked);
  }, [allChecked]);

  return (
    <input
      className="ck"
      type="checkbox"
      value={valueId}
      onChange={() => setSelfChecked(!selfChecked)}
      checked={selfChecked}
    />
  );
};
export default CheckBox;
