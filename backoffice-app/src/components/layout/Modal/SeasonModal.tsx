import { SeasonModalTypes } from "./SeasonModalTypes";
import { _Modal } from "./_Modal";

const SeasonModal = ({
  showModal,
  setShowModal,
  component,
  title,
}: SeasonModalTypes) => {
  return (
    <>
      {showModal ? (
        <_Modal>
          <div
            className="modalBackground"
            onClick={() => setShowModal(false)}
          ></div>

          <div className="modalWrapper">
            <div className="titleContainer">
              <h3>{title}</h3>
            </div>

            <div className="componentContainer">{component}</div>

            {/*<div className="buttonContainer">
              {showButtonSave ? (
                <button
                  className="confirmButton"
                  type="button"
                  style={{ display: `${showButtonSave}` }}
                  onClick={() => createSeason()}
                >
                  Confirm
                </button>
              ) : null}

              {showButtonUpdate ? (
                <button
                  className="confirmButton"
                  type="button"
                  style={{ display: `${showButtonUpdate}` }}
                  onClick={() => updateSeason()}
                >
                  Update
                </button>
              ) : null}

              <button
                className="cancelButton"
                type="button"
                onClick={() => setShowModal(false)}
              >
                Close
              </button>
              </div>*/}
          </div>
        </_Modal>
      ) : null}
    </>
  );
};

export default SeasonModal;
