export interface SeasonModalTypes {
  showButtonUpdate: boolean;
  showButtonSave: boolean;
  showModal: boolean;
  setShowModal: Function;
  createSeason: Function;
  updateSeason: Function;
  title: string;
  component: any;
}
