import axios from "axios";

import { useCallback, useState } from "react";
import { seasonsGetByIdURL } from "../../../utils/apiPaths";
import {
  validationSeason,
  validationSeasonUpdate,
} from "../../../utils/validationSeason";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { CreateOrUpdateSeasonTypes } from "./CreateOrUpdateSeasonTypes";
import { _CreateOrUpdate } from "./_CreateOrUpdate";

const CreateOrUpdateSeason = ({
  isEdit,
  seasonId,
  seasonName,
  setSeasonName,
  dateStart,
  setDateStart,
  dateEnd,
  setDateEnd,
  errorMessage,
  showButtonSave,
  showButtonUpdate,
  setErrorMessage,
  createSeason,
  updateSeason,
  showModal,
  currentSeason,
}: CreateOrUpdateSeasonTypes) => {
  const [error, setError] = useState<boolean>();

  const getSeasonById = useCallback(async () => {
    axios
      .get(seasonsGetByIdURL(seasonId))
      .then((response) => {
        console.log("response: ", response);
        setSeasonName(response.data.name);
      })
      .catch(function (error) {});
  }, [seasonId, setSeasonName]);
  //validate when input is null on save
  const handleSubmmit = (e: any) => {
    e.preventDefault();
    validationSeason({
      seasonName,
      dateStart,
      dateEnd,
      setError,
      errorMessage,
      setErrorMessage,
      createSeason,
    });
  };
  // validate when input is null on Update
  const handleUpdate = (e: any) => {
    e.preventDefault();
    validationSeasonUpdate({
      seasonName,
      dateStart,
      dateEnd,
      setError,
      errorMessage,
      setErrorMessage,
      updateSeason,
    });
  };

  return !isEdit ? (
    <_CreateOrUpdate>
      <form className="form" onSubmit={handleSubmmit}>
        <label htmlFor="seasonName">Season Name</label>
        {!error ? (
          <>
            <input
              type="text"
              placeholder="Season"
              value={seasonName}
              onChange={(e) => setSeasonName(e.target.value)}
            />
          </>
        ) : (
          <>
            <input
              className="errorInput"
              type={"text"}
              placeholder={"Season"}
              value={seasonName}
              onChange={(e) => setSeasonName(e.target.value)}
            />
            <article className="errorMessage">{errorMessage}</article>
          </>
        )}
        <div>
          From:
          <DatePicker
            selected={dateStart}
            onChange={(startDate: Date) => setDateStart(startDate)}
          />
          To:
          <DatePicker
            selected={dateEnd}
            onChange={(endDate: Date) => setDateEnd(endDate)}
          />
        </div>
        <div className="buttonContainer">
          {showButtonSave && (
            <button className="confirmButton" type="submit">
              Confirm
            </button>
          )}
          <button
            className="cancelButton"
            type="button"
            onClick={function () {
              showModal();
            }}
          >
            Close
          </button>
        </div>
      </form>
    </_CreateOrUpdate>
  ) : (
    <_CreateOrUpdate>
      <form className="form" onSubmit={handleUpdate}>
        <label htmlFor="seasonName">Season Name</label>
        {!error ? (
          <>
            <input
              type="text"
              placeholder={currentSeason?.name}
              value={seasonName}
              onChange={(e) => setSeasonName(e.target.value)}
            />
          </>
        ) : (
          <>
            <input
              className="errorInput"
              type="text"
              placeholder={currentSeason?.name}
              value={seasonName}
              onChange={(e) => setSeasonName(e.target.value)}
            />
            <article className="errorMessage">{errorMessage}</article>
          </>
        )}
        <div>
          From:
          <DatePicker
            selected={new Date(dateStart)}
            onChange={(startDate: Date) => setDateStart(startDate)}
          />
          To:
          <DatePicker
            selected={new Date(dateEnd)}
            onChange={(endDate: Date) => setDateEnd(endDate)}
          />
        </div>
        <div className="buttonContainer">
          {showButtonUpdate ? (
            <button className="confirmButton" type="submit">
              Confirm
            </button>
          ) : null}
          <button
            className="cancelButton"
            type="button"
            onClick={function () {
              showModal();
            }}
          >
            Close
          </button>
        </div>
      </form>
    </_CreateOrUpdate>
  );
};

export default CreateOrUpdateSeason;
