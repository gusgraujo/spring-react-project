import { _DeleteModal } from "./_DeleteModal";

export const Delete = (props: any) => {
  const { showModal, setShowModal, user, deleteUser, value } = props;

  return (
    <>
      {showModal ? (
        <_DeleteModal>
          <div
            className="modalBackground"
            onClick={() => setShowModal(false)}
          ></div>
          <div className="modalWrapper">
            <h3 className="titleContainer">Delete User</h3>

            <p>Are you sure that you want to delete user "{user}" ?</p>
            <div className="buttonContainer">
              {deleteUser ? (
                <button
                  className="confirmButton"
                  onClick={() => {
                    deleteUser(value);
                  }}
                >
                  Confirm
                </button>
              ) : null}
              <button
                className="cancelButton"
                onClick={() => setShowModal(false)}
              >
                Close
              </button>
            </div>
          </div>
        </_DeleteModal>
      ) : null}
    </>
  );
};
