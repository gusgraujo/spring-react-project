import { SeasonFields } from "../../Season/SeasonTypes";

export interface CreateOrUpdateSeasonTypes {
  isEdit: boolean;
  seasonId: number;
  seasonName: string;
  setSeasonName: Function;
  dateStart: Date;
  setDateStart: Function;
  dateEnd: Date;
  setDateEnd: Function;
  border: string;
  setBorder: Function;
  errorMessage: string;
  setErrorMessage: Function;
  createSeason: Function;
  updateSeason: Function;
  showButtonSave: boolean;
  showButtonUpdate: boolean;
  showModal: Function
  currentSeason: SeasonFields | undefined;
  }