export interface DeleteSeasonTypes {
  showModal: boolean;
  season: {
    id: number;
    name: string;
  };
  setShowModal: Function;
  deleteSeason: Function;
}
