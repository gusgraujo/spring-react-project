import styled from "styled-components";

export const _CreateOrUpdate = styled.section`
  .cancelButton {
    background-color: #3182ce;
  }

  .confirmButton {
    background-color: #38a169;
    margin-right: 1.5rem;
  }

  .cancelButton,
  .confirmButton {
    color: white;
    padding: 0.5rem;
    padding-right: 0.7rem;
    padding-left: 0.7rem;
    border-radius: 5px;
    font-family: "CanaroSemiBold";
  }

  .buttonContainer {
    display: flex;
    justify-content: end;
    align-items: flex-end;
    margin-top: 20px;
  }

  .errorInput {
    width: 100%;
    margin-top: 10px;
    margin-bottom: 4px;
    border: 2px solid #ed7a7a;
    height: 2.5rem;
    padding: 10px;
    border-radius: 5px;
  }

  .errorMessage {
    color: #ed7a7a;
    font-family: "CanaroBook";
    font-size: 15px;
    margin-left: 5px;
  }
`;
