import { DeleteSeasonTypes } from "./DeleteSeasonTypes";
import { _DeleteModal } from "./_DeleteModal";

const DeleteSeason = ({
  showModal,
  setShowModal,
  season,
  deleteSeason,
}: DeleteSeasonTypes) => {
  return (
    <>
      {showModal && (
        <_DeleteModal>
          <div
            className="modalBackground"
            onClick={() => setShowModal(false)}
          ></div>

          <div className="modalWrapper">
            <h3 className="titleContainer">Delete Season</h3>
            <p>Are you sure that you want to delete season "{season.name}" ?</p>

            <div className="buttonContainer">
              <button className="confirmButton" onClick={() => deleteSeason()}>
                Confirm
              </button>

              <button
                className="cancelButton"
                onClick={() => setShowModal(false)}
              >
                Close
              </button>
            </div>
          </div>
        </_DeleteModal>
      )}
    </>
  );
};

export default DeleteSeason;
