import styled from "styled-components";

export const _DeleteModal = styled.section`
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;

  .modalBackground {
    opacity: 0.4;
    background-color: rgb(0 0 0 / 1);
    width: 100%;
    height: 100%;
    position: fixed;
  }

  .modalWrapper {
    position: relative;
    background-color: white;
    padding: 1.5rem;
    max-width: 32rem;
    width: 100%;
    height: 14rem;
    border-radius: 0.375rem;
  }

  .cancelButton {
    background-color: #6495ed;
  }

  .confirmButton {
    background-color: #cd5c5c;
    margin-right: 1.5rem;
  }

  .cancelButton,
  .confirmButton {
    color: white;
    padding: 0.5rem;
    padding-right: 0.7rem;
    padding-left: 0.7rem;
    border-radius: 5px;
    font-family: "CanaroSemiBold";
  }

  .buttonContainer {
    display: flex;
    justify-content: end;
    bottom: 0;
    height: 5rem;
    align-items: flex-end;
    align-self: end;
  }

  .titleContainer {
    font-size: 22px;
    font-family: "CanaroSemiBold";
    margin-bottom: 1rem;
  }
`;
