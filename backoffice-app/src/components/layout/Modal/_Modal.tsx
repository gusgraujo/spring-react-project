import styled from "styled-components";

export const _Modal = styled.section`
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;

  .modalBackground {
    opacity: 0.4;
    background-color: rgb(0 0 0 / 1);
    width: 100%;
    height: 100%;
    position: fixed;
  }

  .modalWrapper {
    position: relative;
    background-color: white;
    padding: 1.2rem;
    max-width: 25rem;
    width: 100%;
    height: fit-content;
    border-radius: 0.375rem;
  }

  .titleContainer {
    font-size: 22px;
    font-family: "CanaroBold";
    margin-bottom: 1rem;
  }

  input, select {
    width: 100%;
    margin-top: 10px;
    margin-bottom: 10px;
    border: 1px solid #b3b2b2;
    color: #000;
    height: 2.5rem;
    padding: 10px;
    border-radius: 5px;
  }

  input:placeholder {
    color: #b3b2b2;
  }
`;
