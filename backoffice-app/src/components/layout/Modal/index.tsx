import { _Modal } from "./_Modal";

const Modal = (prop: any) => {
  const { showModal, setShowModal,showTitle } = prop;

  return (
    <>
      {showModal ? (
        <_Modal>
          <div
            className="modalBackground"
            onClick={() => setShowModal(false)}
          ></div>

          <div className="modalWrapper">
            <div className="titleContainer">
              {showTitle ? <h3>{prop.title}</h3>: <h3>{prop.titleUdate}</h3>}
            </div>
            <div className="componentContainer">{prop.component}</div>

            {/* <div className="buttonContainer">
              {prop.saveUser ? (
                <button
                  className="confirmButton"
                  type="button"
                  style={{ display: `${showButtonSave}` }}
                  onClick={function () {
                    saveUser();
                  }}
                >
                  Confirm
                </button>
              ) : null}
              {updateUser ? (
                <button
                  className="confirmButton"
                  type="button"
                  style={{ display: `${showButtonUpdate}` }}
                  onClick={function () {
                    updateUser(valor);
                  }}
                >
                  Confirm
                </button>
              ) : null}
              <button
                className="cancelButton"
                type="button"
                onClick={() => setShowModal(false)}
              >
                Close
              </button>
                </div>*/}
          </div>
        </_Modal>
      ) : null}
    </>
  );
};

export default Modal;
