import styled from "styled-components";

export const _MovieTitle = styled.section`
  display: grid;
  grid-template-columns: 1fr 5fr 5fr 1fr 2fr;
  align-items: end;
  font-family: "CanaroSemibold";
  font-size: 17px;
  border-bottom: 2px solid black;

  & > section {
    padding-top: 1rem;
    padding-bottom: 1rem;
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    justify-self: center;
  }

  & > section:first-child {
    justify-self: start;
  }

  & > section:last-child {
    justify-self: end;
  }

  .sortButton {
    display: flex;
  }
`;
