import { BsTrash } from "react-icons/bs";
import { FaRegEdit } from "react-icons/fa";
import CheckBox from "../Checkbox/CheckBox";
import { movieResponseData } from "./interfacesMovies";

interface Props {
  movie: movieResponseData;
  toggleEditModal: Function;
  toggleDeleteModal: Function;
}

const MovieTableItem = (props: Props) => {
  const { movie, toggleEditModal, toggleDeleteModal } = props;
  return (
    <section className="movieTableRow">
      <section>
        <CheckBox />
      </section>
      <section>
        <article>{movie.name}</article>
      </section>
      <section>
        <article>{movie.participants[0].name}</article>
      </section>
      <section>
        <article>{movie.status.toString()}</article>
      </section>
      <section>
        <button
          onClick={() => {
            toggleEditModal({
              id: movie.id,
              idMovie: movie.idMovie,
              participantId: movie.participants.at(0)?.id ?? 0,
              status: movie.status,
              background: movie.background ?? "",
            });
          }}
        >
          <FaRegEdit color="#1564f1" size={35} />
        </button>
        <button
          onClick={() => {
            toggleDeleteModal({
              id: movie.id,
              movieTitle: movie.name,
              participantName: movie.participants.at(0)?.name ?? ``,
            });
          }}
        >
          <BsTrash color="#f90101" size={35} />
        </button>
      </section>
    </section>
  );
};

export default MovieTableItem;
