import { SyntheticEvent } from "react";
import { infoForEditModal } from "../interfacesMovies";
import { _CreateOrUpdate } from "../../layout/Modal/_CreateOrUpdate";
import { _Modal } from "../../layout/Modal/_Modal";
import { UserFields } from "../../User/UserTypes";

interface Props {
  closeModals: () => void;
  movieUpdatePost: (event: SyntheticEvent) => void;
  infoForEdit: infoForEditModal | undefined;
  setInfoForEdit: (data: infoForEditModal) => void;
  participantsList: Array<UserFields>;
}

const EditMovieModal = (props: Props) => {
  const {
    closeModals,
    movieUpdatePost,
    infoForEdit,
    setInfoForEdit,
    participantsList,
  } = props;

  return (
    <_Modal>
      <div className="modalBackground" onClick={closeModals}></div>

      <div className="modalWrapper">
        <div className="titleContainer">
          <h3>Edit</h3>
        </div>

        <div className="componentContainer">
          <_CreateOrUpdate>
            <form className="form" onSubmit={movieUpdatePost}>
              <label htmlFor="imdbID">IMDB Id</label>
              <input
                id="imdbID"
                type="text"
                placeholder="IMDB ID"
                value={infoForEdit?.idMovie}
                onChange={(e) =>
                  setInfoForEdit({
                    ...infoForEdit,
                    idMovie: e.target.value,
                  })
                }
              />

              <label htmlFor="participantId">Participant</label>
              <select
                id="participantId"
                defaultValue={infoForEdit?.participantId}
                onChange={(e) =>
                  setInfoForEdit({
                    ...infoForEdit,
                    participantId: +e.target.value,
                  })
                }
              >
                {participantsList.map((participant, index) => (
                  <option key={`ParticList_${index}`} value={participant.id}>
                    {participant.name} - {participant.discordName}
                  </option>
                ))}
              </select>

              <label htmlFor="background">Background URL</label>
              <input
                id="background"
                type="text"
                placeholder="Background URL"
                value={infoForEdit?.background}
                onChange={(e) =>
                  setInfoForEdit({
                    ...infoForEdit,
                    background: e.target.value,
                  })
                }
              />

              <input
                type="checkbox"
                name="status"
                id="status"
                defaultChecked={infoForEdit?.status}
                onChange={(e) =>
                  setInfoForEdit({
                    ...infoForEdit,
                    status: e.target.checked,
                  })
                }
              />
              <label htmlFor="status">Status</label>

              {/*{errorMessage && (
                      <div className="text-[red]">{errorMessage}</div>
                    )}*/}

              <div className="buttonContainer">
                <button className="confirmButton" type="submit">
                  Confirm
                </button>
                <button
                  className="cancelButton"
                  type="button"
                  onClick={closeModals}
                >
                  Close
                </button>
              </div>
            </form>
          </_CreateOrUpdate>
        </div>
      </div>
    </_Modal>
  );
};

export default EditMovieModal;
