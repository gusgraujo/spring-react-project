import { infoForDeleteModal } from "../interfacesMovies";
import { _DeleteModal } from "../../layout/Modal/_DeleteModal";

interface Props {
  closeModals: () => void;
  infoForDelete: infoForDeleteModal | undefined;
  movieDelete: () => void;
}

const DeleteMovieModal = (props: Props) => {
  const { closeModals, infoForDelete, movieDelete } = props;
  return (
    <_DeleteModal>
      <div className="modalBackground" onClick={closeModals}></div>

      <div className="modalWrapper">
        <h3 className="titleContainer">Delete Season</h3>
        <p>
          Are you sure that you want to delete the movie "
          {infoForDelete?.movieTitle}", suggested by "
          {infoForDelete?.participantName}" ?
        </p>

        <div className="buttonContainer">
          <button className="confirmButton" onClick={movieDelete}>
            Confirm
          </button>

          <button className="cancelButton" onClick={closeModals}>
            Close
          </button>
        </div>
      </div>
    </_DeleteModal>
  );
};

export default DeleteMovieModal;
