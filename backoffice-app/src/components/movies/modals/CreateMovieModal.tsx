import { SyntheticEvent } from "react";
import { _CreateOrUpdate } from "../../layout/Modal/_CreateOrUpdate";
import { _Modal } from "../../layout/Modal/_Modal";
import { UserFields } from "../../User/UserTypes";

interface Props {
  movieCreationPost: (event: SyntheticEvent) => void;
  setMovieIMDBId: (data: string) => void;
  setParticipantId: (data: number) => void;
  closeModals: () => void;
  participantId: number;
  participantsList: Array<UserFields>;
}

const CreateMovieModal = (props: Props) => {
  const {
    movieCreationPost,
    setMovieIMDBId,
    setParticipantId,
    closeModals,
    participantId,
    participantsList,
  } = props;

  return (
    <_Modal>
      <div className="modalBackground" onClick={closeModals}></div>

      <div className="modalWrapper">
        <div className="titleContainer">
          <h3>Create</h3>
        </div>

        <div className="componentContainer">
          <_CreateOrUpdate>
            <form className="form" onSubmit={movieCreationPost}>
              <label htmlFor="imdbID">IMDB Id</label>
              <input
                id="imdbID"
                type="text"
                placeholder="IMDB ID"
                onChange={(e) => setMovieIMDBId(e.target.value)}
              />

              <label htmlFor="participantId">Participant</label>
              <select
                id="participantId"
                defaultValue={participantId}
                onChange={(e) => setParticipantId(+e.target.value)}
              >
                {participantsList.map((participant, index) => (
                  <option key={`ParticList_${index}`} value={participant.id}>
                    {participant.name} - {participant.discordName}
                  </option>
                ))}
              </select>

              {/*{errorMessage && (
                      <div className="text-[red]">{errorMessage}</div>
                    )}*/}

              <div className="buttonContainer">
                <button className="confirmButton" type="submit">
                  Confirm
                </button>
                <button
                  className="cancelButton"
                  type="button"
                  onClick={closeModals}
                >
                  Close
                </button>
              </div>
            </form>
          </_CreateOrUpdate>
        </div>
      </div>
    </_Modal>
  );
};

export default CreateMovieModal;
