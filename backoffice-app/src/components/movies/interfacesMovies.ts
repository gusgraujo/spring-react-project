import { UserFields } from "../User/UserTypes";

export interface infoForEditModal {
  id?: number;
  idMovie?: string;
  participantId?: number;
  background?: string | undefined;
  status?: boolean;
}
export interface infoForDeleteModal {
  id: number;
  movieTitle: string;
  participantName: string;
}

export interface movieResponseData {
  id: number;
  name: string;
  idMovie: string;
  status: boolean;
  participants: Array<UserFields>;
  background: string | undefined;
}
