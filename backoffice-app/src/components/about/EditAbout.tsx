import EditAboutTypes from "./EditAboutTypes";

const EditAbout = ({ text }: EditAboutTypes) => {
  return <p>{text}</p>;
};

export default EditAbout;
