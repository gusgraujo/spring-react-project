import {
  BsChevronDoubleLeft,
  BsChevronDoubleRight,
  BsChevronLeft,
  BsChevronRight,
} from "react-icons/bs";
import { StyledPagination } from "./StyledPagination";

interface Props {
  currentPage: number;
  totalPages: number;
  setCurrentPage: Function;
}

const PaginationTest = (props: Props) => {
  const { currentPage, setCurrentPage, totalPages } = props;

  function handleDoubleBackPage() {
    if (currentPage <= 5) {
      setCurrentPage(1);
    } else {
      setCurrentPage((prevState: number) => prevState - 5);
    }
  }

  function handleBack() {
    if (currentPage === 1) return;

    setCurrentPage(currentPage - 1);
  }

  function handleForward() {
    if (currentPage === totalPages) return;

    setCurrentPage(currentPage + 1);
  }

  function handleDoubleForwardPage() {
    if (currentPage + 5 > totalPages) {
      setCurrentPage(totalPages);
    } else setCurrentPage((prevState: number) => prevState + 5);
  }

  return (
    <StyledPagination>
      <button onClick={handleDoubleBackPage}>
        <BsChevronDoubleLeft />
      </button>
      <button onClick={() => handleBack()}>
        <BsChevronLeft />
      </button>
      <span style={{ padding: "6px" }}>
        Page {currentPage} of {totalPages}
      </span>

      <button onClick={() => handleForward()}>
        <BsChevronRight />
      </button>
      <button>
        <BsChevronDoubleRight onClick={handleDoubleForwardPage} />
      </button>
    </StyledPagination>
  );
};

export default PaginationTest;
