import { useEffect, useState } from "react";
import { AiFillHome } from "react-icons/ai";
import { BsCameraReelsFill } from "react-icons/bs";
import { FaCalendarAlt, FaUsers } from "react-icons/fa";
import { ImTrophy } from "react-icons/im";
import { Link, useLocation } from "react-router-dom";
import styled from "styled-components";
import { StyledNavbar } from "./StyledNavbar";
import "./styles.css";

const StyledButton = styled.button`
  padding: 8px;
  border: 1px solid white;
  border-radius: 4px;
  &:hover {
    color: white;
  }
`;

const Header = () => {
  const [currentPage, setCurrentPage] = useState<any>();
  const location = useLocation();

  function handleLogout() {
    localStorage.clear();
    window.location.reload();
  }

  useEffect(() => {
    setCurrentPage(location.pathname);
  }, [currentPage, location]);

  return (
    <StyledNavbar>
      <article className="appTitle">BackOffice</article>
      <section className="links">
        <Link to="/" className={currentPage === "/" ? "linksGlow" : ""}>
          <AiFillHome
            size={25}
            className={currentPage === "/" ? "icon-glow" : ""}
          />
          Home
        </Link>
        <Link
          to="/movies"
          className={currentPage === "/movies" ? "linksGlow" : ""}
        >
          <BsCameraReelsFill
            size={25}
            className={currentPage === "/movies" ? "icon-glow" : ""}
          />
          Movies
        </Link>
        <Link
          to={"/seasons"}
          className={currentPage === "/seasons" ? "linksGlow" : ""}
        >
          <FaCalendarAlt
            size={25}
            className={currentPage === "/seasons" ? "icon-glow" : ""}
          />
          Seasons
        </Link>
        <Link
          to={"/participants"}
          className={currentPage === "/participants" ? "linksGlow" : ""}
        >
          <FaUsers
            size={25}
            className={currentPage === "/participants" ? " icon-glow" : ""}
          />
          Participants
        </Link>
        <Link
          to="/awards"
          className={currentPage === "/awards" ? "linksGlow" : ""}
        >
          <ImTrophy
            size={25}
            className={currentPage === "/awards" ? " icon-glow" : ""}
          />
          Awards
        </Link>
      </section>

      <StyledButton onClick={handleLogout}>Logout</StyledButton>
    </StyledNavbar>
  );
};

export default Header;
