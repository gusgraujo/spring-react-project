import styled from "styled-components";

export const StyledNavbar = styled.section`
  background-color: #4e4e4e;
  display: grid;
  grid-template-columns: 1fr 4fr 1fr;
  font-family: "CanaroBook";
  color: #8e8e8e;
  box-shadow: 0px 15px 19px #fff;
  justify-items: center;
  align-items: center;
  width: 100%;
  padding: 1.5rem;
  height: 8rem;

  .appTitle {
    justify-self: center;
    font-size: 3rem;
    line-height: 1;
    color: #fff;
  }

  .links {
    font-family: "CanaroLight";
    font-size: 1.25rem;
    line-height: 1.75rem;
    width: 66.666667%;
    display: flex;
    justify-content: space-around;
    align-items: center;
  }

  .flex {
    display: flex;
  }

  .linksGlow {
    text-shadow: 0 0 6px #fff, 0 0 6px #fff, 0 0 2px;
    font-weight: 700;
    color: white;
  }

  .iconGlow {
    filter: drop-shadow(0px 0px 2px #fff);
  }

  .links > a {
    display: flex;
  }

  a > svg {
    margin-right: 0.5rem;
  }
`;
