import styled from "styled-components";

export const _UserImage = styled.figure`
  height: auto;
  width: 3rem;
  border-radius: 25px;
  overflow: hidden;
`;
