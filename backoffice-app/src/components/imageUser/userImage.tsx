import { _UserImage } from "./_UserImage";

const UserImage = (props: any) => {
  return (
    <_UserImage>
      <img src={props.avatar} alt="" />
    </_UserImage>
  );
};
export default UserImage;
