export interface UserTypes {
  content: [
    UserFields
  ];
};

export interface UserFields {
  id: number;
  name: string;
  discordName: string;
  statusDraw: boolean;
}
