import { useRef, ChangeEvent } from "react";
import { _User } from "./StyledUser";
import { SpinnerRoundFilled } from "spinners-react";
import { Cropper, CropperRef } from "react-advanced-cropper";
import "react-advanced-cropper/dist/style.css";
import { createUserValidation,editUserValidation } from "../../utils/validation";


const User = (props: any) => {
  const inputRef: any = useRef<HTMLInputElement>();
  const cropperRef: any = useRef<CropperRef>();

  const {
    setNome,
    name,
    update,
    usernameError,
    discordError,
    setDiscordError,
    setErrorMessage,
    spiner,
    preview,
    saveUser,
    updateUser,
    exit,
    discordName,
    setPreview,
    setDiscordName,
  } = props;

  const onUpload = () => {
    if (inputRef.current) {
      inputRef.current.click();
    }
  };

  const onCrop = () => {
    const cropper = cropperRef.current;
    if (cropper) {
      const canvas: any = cropper.getCanvas();
      const base64: string = canvas.toDataURL("image/png")
      console.log("BASE", base64)
      props.setCroppedAvatar(base64);
    }
  };

  const onLoadImage = (event: ChangeEvent<HTMLInputElement>) => {
    if (!event.target.files) return;
    setPreview(URL.createObjectURL(event.target.files[0]));
  };

  const handleEdit: any = (e: Event) => {
    e.preventDefault();
    editUserValidation({
      discordName,
      name,
      usernameError,
      discordError,
      setErrorMessage,
      setDiscordError,
      updateUser,
    });
  };

  const handleSubmmit: any = (e: any) => {
    e.preventDefault();
    createUserValidation({
      discordName,
      name,
      usernameError,
      discordError,
      setErrorMessage,
      setDiscordError,
      saveUser,
    });
  };

  return (
    <>
      <_User>
        <SpinnerRoundFilled
          size={50}
          thickness={100}
          speed={100}
          color="rgba(57, 72, 172, 1)"
          className="spiner"
          enabled={spiner}
        />
      </_User>
      {update===true ? (
        <_User>
          <form onSubmit={handleSubmmit} onClick={onCrop} className="form">
            <label>User Name</label>
            {!usernameError ? (
              <input
                id="username"
                value={name || ""}
                onChange={(event) => setNome(event.target.value)}
                type="text"
                placeholder="Username"
              />
            ) : (
              <>
                <input
                  className="errorInput"
                  id="username"
                  value={name || ""}
                  onChange={(event) => setNome(event.target.value)}
                  type="text"
                  placeholder="Username"
                />
                <article className="errorMessage">{usernameError}</article>
              </>
            )}

            <label>DiscordTag</label>
            {!discordError ? (
              <input
                value={discordName || ""}
                onChange={(event) => setDiscordName(event.target.value)}
                type="text"
                placeholder="DiscordTag"
              />
            ) : (
              <>
                <input
                  className="errorInput"
                  value={discordName || ""}
                  onChange={(event) => setDiscordName(event.target.value)}
                  type="text"
                  placeholder="DiscordTag"
                />
                <article className="errorMessage">{discordError}</article>
              </>
            )}
            <label htmlFor="username">User Photo</label>
            <div className="photoContainer">
              <label className="choosePhoto" htmlFor="ChosePhoto">
                Choose Photo
              </label>
              <button className="example__button" onClick={onUpload}>
                <input
                  id="ChosePhoto"
                  ref={inputRef}
                  type="file"
                  accept="image/*"
                  onChange={onLoadImage}
                  style={{ display: "none" }}
                />
              </button>
            </div>
            <div className="photoContainer">
              <Cropper
                ref={cropperRef}
                className="photoCrop"
                backgroundClassName="bg"
                src={preview || null}
                canvas={true}
              />
            </div>

            <div className="buttonContainer">
              <button
                type="submit"
                className="confirmButton" >
          
                Confirm
              </button>
              <button
                type="button"
                className="cancelButton"
                onClick={function () {
                  exit();
                }}
              >
                Close
              </button>
            </div>
          </form>
        </_User>
      ) : (
        <_User>
          <form onSubmit={handleEdit} onClick={onCrop} className="form" name="update">
            <label>User Name</label>
            {!usernameError ? (
              <input
                id="username"
                value={name || ""}
                onChange={(event) => setNome(event.target.value)}
                type="text"
                placeholder="Username"
              />
            ) : (
              <>
                <input
                  className="errorInput"
                  id="username"
                  value={name || ""}
                  onChange={(event) => setNome(event.target.value)}
                  type="text"
                  placeholder="Username"
                />
                <article className="errorMessage">{usernameError}</article>
              </>
            )}

            <label>DiscordTag</label>
            {!discordError ? (
              <input
                value={discordName || ""}
                onChange={(event) => setDiscordName(event.target.value)}
                type="text"
                placeholder="DiscordTag"
              />
            ) : (
              <>
                <input
                  className="errorInput"
                  value={discordName || ""}
                  onChange={(event) => setDiscordName(event.target.value)}
                  type="text"
                  placeholder="DiscordTag"
                />
                <article className="errorMessage">{discordError}</article>
              </>
            )}

            <label htmlFor="username">User Photo</label>
            <div className="photoContainer">
              <label className="choosePhoto" htmlFor="ChosePhoto">
                Choose Photo
              </label>
              <button className="example__button" onClick={onUpload}>
                <input
                  id="ChosePhoto"
                  ref={inputRef}
                  type="file"
                  accept="image/*"
                  onChange={onLoadImage}
                  style={{ display: "none" }}
                />
              </button>
            </div>
            <div className="photoContainer">
              <Cropper
                ref={cropperRef}
                className="photoCrop"
                backgroundClassName="bg"
                src={preview || null}
                canvas={true}
              />
            </div>

            <div className="buttonContainer">
              <button className="confirmButton" type="submit" >
                Update
              </button>
              <button
                type="button"
                className="cancelButton"
                onClick={function () {
                  exit();
                }}
              >
                Close
              </button>
            </div>
          </form>
        </_User>
      )}
    </>
  );

};

export default User;
