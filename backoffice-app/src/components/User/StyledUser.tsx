import styled from "styled-components";

export const _User = styled.section`
  & > label,
  .choosePhoto {
    font-family: "CanaroSemibold";
  }
  .spiner {
    margin: 0px 180px;
  }
  .photoContainer {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .photoCrop {
    object-fit: cover;
    width: 100%;
    height: 250px;
    background-color: white;
  }
  .bg {
    background-color: white;
  }

  .choosePhoto {
    padding: 0.5rem;
    color: #000000;
    border: 2px solid #9ca3af;
    border-radius: 10px;
    width: fit-content;
    margin-bottom: 0.5rem;
    margin-top: 0.5rem;
  }
  .cancelButton {
    background-color: #6495ed;
  }

  .confirmButton {
    background-color: #38a169;
    margin-right: 1.5rem;
  }

  .cancelButton,
  .confirmButton {
    color: white;
    padding: 0.5rem;
    padding-right: 0.7rem;
    padding-left: 0.7rem;
    border-radius: 5px;
    font-family: "CanaroSemiBold";
  }

  .buttonContainer {
    display: flex;
    justify-content: end;
    align-items: flex-end;
    margin-top: 40px;
  }

  .validationError {
    color: #e93e3e;
  }

  .errorInput {
    width: 100%;
    margin-top: 10px;
    margin-bottom: 4px;
    border: 2px solid #ed7a7a;
    height: 2.5rem;
    padding: 10px;
    border-radius: 5px;
  }

  .errorMessage {
    color: #ed7a7a;
    font-family: "CanaroBook";
    font-size: 15px;
    margin-left: 5px;
    margin-bottom: 10px;
  }
`;
