export interface ToggleSliderTypes {
  statusDraw: boolean;
  updateStatusDraw: Function;
  userId: number;
}
