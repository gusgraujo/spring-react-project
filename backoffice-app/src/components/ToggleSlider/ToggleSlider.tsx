import { _ToggleSlider } from "./StyledToggleSlider";
import { ToggleSliderTypes } from "./ToggleSliderTypes";

const ToggleSlider = ({
  statusDraw,
  updateStatusDraw,
  userId,
}: ToggleSliderTypes) => {
  return (
    <_ToggleSlider>
      <input
        type="checkbox"
        checked={statusDraw}
        onChange={() => updateStatusDraw(userId, statusDraw)}
      />
      <span className="slider" />
    </_ToggleSlider>
  );
};

export default ToggleSlider;
