import styled from "styled-components";

export const StyledPagination = styled.div`
  margin: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
