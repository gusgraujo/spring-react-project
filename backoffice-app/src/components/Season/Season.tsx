import axios from "axios";
import { useState } from "react";
import { BsTrash } from "react-icons/bs";
import { FaRegEdit, FaSearch } from "react-icons/fa";
import {
  seasonsCreateURL,
  seasonsDeleteURL,
  seasonsUpdateURL,
} from "../../utils/apiPaths";
import CheckBox from "../Checkbox/CheckBox";
import CreateOrUpdateSeason from "../layout/Modal/CreateOrUpdateSeason";
import DeleteSeason from "../layout/Modal/DeleteSeason";
import SeasonModal from "../layout/Modal/SeasonModal";
import Title from "../layout/Title";
import SeasonTableTitle from "../layout/Title/SeasonTableTitle";
import { SeasonFields, SeasonTypes } from "./SeasonTypes";
import { StyledSeason } from "./StyledSeason";
import { format } from 'date-fns'


interface Props {
  seasonList: SeasonTypes;
  sort: boolean;
  search: string;
  toggleSort: () => void;
  refreshContent: () => void;
  handleSearch: Function;
}

export const Season = (props: Props) => {
  const { seasonList, sort, toggleSort, refreshContent, handleSearch } = props;
  const [allChecked, setAllChecked] = useState<boolean>(false);
  const [currentSeasonModal, setCurrentSeasonModal] = useState<any>();
  const [edit, setEdit] = useState<boolean>(false);
  const [border, setBorder] = useState<string>("");
  const [errorMessage, setErrorMessage] = useState<string>("");

  const [seasonName, setSeasonName] = useState<string>("");
  const [dateStart, setDateStart] = useState(new Date());
  const [dateEnd, setDateEnd] = useState(new Date());

  const [seasonShowSeasonModal, setShowSeasonModal] = useState<boolean>(false);
  const [toggleDeleteSeasonModal, setToggleDeleteSeasonModal] =
    useState<boolean>(false);
  const [seasonClicked, setSeasonClicked] = useState<SeasonFields>();

  const toggleDeleteModalOnClick = (season: any) => {
    setCurrentSeasonModal(season);
    setToggleDeleteSeasonModal(true);
  };

  const createSeason = async () => {
    const season: any = {
      name: seasonName,
      dateStart: dateStart.toJSON(),
      dateEnd: dateEnd.toJSON()
    };
    console.log(season)
    await axios({
      method: "POST",
      url: seasonsCreateURL,
      data: season,
    }).then(() => {
      setShowSeasonModal(false);
      setSeasonName("");
      setDateStart(new Date());
      setDateEnd(new Date());
      refreshContent();
    });
  };

  const updateSeason = async (id: number) => {
    const season: any = {
      name: seasonName,
      dateStart: dateStart,
      dateEnd: dateEnd
    };
    
    await axios({
      method: "PUT",
      url: seasonsUpdateURL(id),
      data: season,
    }).then(() => {
      setShowSeasonModal(false);
      setSeasonName("");
      setDateStart(new Date());
      setDateEnd(new Date());
      refreshContent();
    });
  };

  const deleteSeason = async (id: number) => {
    axios
      .delete(seasonsDeleteURL(id))
      .then(() => {
        setToggleDeleteSeasonModal(false);
        refreshContent();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  //close Modal
  const close = () => setShowSeasonModal(false);

  const toggleEditModal = (toggleEdit: boolean, season: any) => {
    setSeasonName("");
    setDateStart(new Date());
    setDateEnd(new Date());
    setBorder("");
    setErrorMessage("");
    setCurrentSeasonModal(season);
    setShowSeasonModal(true);
    setSeasonClicked(season);
    setSeasonName(season.name);
    setDateStart(season.dateStart);
    setDateEnd(season.dateEnd);
    if (toggleEdit) {
      setEdit(false);
    } else {
      setEdit(true);
    }
  };

  return (
    <StyledSeason>
      <div className="pageHeader">
        <section>
          <Title title="Seasons" />
        </section>
        <section className="searchAddNewContainer">
          <section className="searchContainer">
            <FaSearch size={15} />
            <input
              type={"text"}
              className="search"
              onChange={(event) => handleSearch(event.target.value)}
              placeholder="Search User"
            />
          </section>
          <button
            className="addNewButton"
            type="button"
            onClick={() => toggleEditModal(true, "")}
          >
            Add New
          </button>
        </section>
      </div>
      <div
        id="datatable"
        data-mdb-selectable="true"
        data-mdb-multi="true"
        className="seasonTable"
      >
        <section className="seasonTableContainer">
          <section>
            <SeasonTableTitle
              name="Name"
              actions="Actions"
              setAllChecked={setAllChecked}
              allChecked={allChecked}
              sort={sort}
              toggleSort={toggleSort}
            />
          </section>
          <section className="seasonList">
            {seasonList &&
              seasonList.content &&
              Object.values(seasonList.content).map((season, index) => {
                return (
                  <section className="seasonTableRow" key={index}>
                    <section>
                      <CheckBox />
                    </section>
                    <section>
                      <article>{season?.name}</article>
                    </section>
                    <section>
                      <button onClick={() => toggleEditModal(false, season)}>
                        <FaRegEdit color="#1564f1" size={35} />
                      </button>
                      <button onClick={() => toggleDeleteModalOnClick(season)}>
                        <BsTrash color="#f90101" size={35} />
                      </button>
                    </section>
                  </section>
                );
              })}
          </section>
        </section>

        <SeasonModal
          setShowModal={setShowSeasonModal}
          showModal={seasonShowSeasonModal}
          title={!edit ? "New Season" : "Edit Season"}
          component={
            <CreateOrUpdateSeason
              isEdit={edit}
              seasonName={seasonName}
              setSeasonName={setSeasonName}
              dateStart={dateStart}
              setDateStart={setDateStart}
              dateEnd={dateEnd}
              setDateEnd={setDateEnd}
              seasonId={currentSeasonModal?.id}
              createSeason={createSeason}
              updateSeason={() => updateSeason(currentSeasonModal?.id)}
              border={border}
              setBorder={setBorder}
              errorMessage={errorMessage}
              setErrorMessage={setErrorMessage}
              showModal={close}
              showButtonSave={!edit}
              showButtonUpdate={edit}
              currentSeason={seasonClicked}
            />
          }
          showButtonUpdate={edit}
          showButtonSave={!edit}
          createSeason={createSeason}
          updateSeason={() => updateSeason(currentSeasonModal?.id)}
        />
        {currentSeasonModal && (
          <DeleteSeason
            showModal={toggleDeleteSeasonModal}
            season={currentSeasonModal}
            setShowModal={setToggleDeleteSeasonModal}
            deleteSeason={() => deleteSeason(currentSeasonModal?.id)}
          />
        )}
      </div>
    </StyledSeason>
  );
};
