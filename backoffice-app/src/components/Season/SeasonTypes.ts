export interface SeasonTypes {
  content: [
    SeasonFields
  ];
};

export interface SeasonFields {
  id: number;
  name: string;
  dateStart: Date;
  dateEnd: Date;
}
