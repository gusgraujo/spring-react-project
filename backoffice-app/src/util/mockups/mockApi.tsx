import axios from "axios";
import { usersGetAllURL, usersGetByIdURL, usersCreateURL, usersDeleteURL, usersUpdateURL } from "../../utils/apiPaths";

// const URL = "https://63d9afe22af48a60a7bdfd40.mockapi.io/api/v1/user";
export const getAllUser = async ()=> {
  const response = await axios.get(usersGetAllURL);
  return response;
}
//getAllUsers




//getUserWithID
export const getUsersWithID = (id: number) => {
  axios
    .get(usersGetByIdURL(id))
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//createUser
export const createUser = (user: Object) => {
  axios
    .post(usersCreateURL, user)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//updateUser
export const updateUser = (id: number, user: Object) => {
  axios
    .put(usersUpdateURL(id), user)
    .then(function (response) {
      console.log(response.data);
    })
     .catch(function (error) {
      console.log(error);
    });
};

//deleteUser
export const deleteUser = (id: number) => {
  axios
    .delete(usersDeleteURL(id))
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};
