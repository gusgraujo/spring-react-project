import { UserFields } from "../components/User/UserTypes";

export const searchFilter = (list: Array<UserFields> | undefined, search: any) => {
  var result: any = list?.filter((user: any) => {
    return (user.name)?.toLowerCase().includes(search.toLowerCase()) || (user.discordName)?.toLowerCase().includes(search.toLowerCase())
  }
  );
  return result;
};
