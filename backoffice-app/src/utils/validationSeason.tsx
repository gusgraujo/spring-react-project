export const validationSeason = ({
  seasonName,
  startDate,
  endDate,
  setError,
  setErrorMessage,
  createSeason,
}: any) => {
  if (seasonName === null || seasonName === "" || startDate === null || endDate === null) {
    setError(true);
    setErrorMessage("The season name and dates cannot be empty");
    return false;
  }

  createSeason();
  setError(false);
  setErrorMessage("");
};
export const validationSeasonUpdate = ({
  seasonName,
  startDate,
  endDate,
  setErrorMessage,
  setError,
  updateSeason,
}: any) => {
  if (seasonName === null || seasonName === "" || startDate === null || endDate === null) {
    setError(true);
    setErrorMessage("The season name and dates cannot be empty");
    return false;
  }

  updateSeason();
  setError(false);
  setErrorMessage("");
};
