import { toast } from "react-toastify";
export const success = () => toast.success("Usuario Adicionado Com Sucesso");
export const errorSuccess = () => toast.error("Erro ao Salvar o Usuario");
export const update = () => toast.success("Usuario Modificado Com Sucesso");
export const errorUpdate = () => toast.error("Erro ao Atualizar o Usuario");
export const deleted = () => toast.warning("Usuario Removido Com Sucesso");
export const errorDeleted = () => toast.error("Erro ao Remover o Usuario");
export const errorImage = () =>
  toast.warn("A Imagem e muito grande esolha outra com menor tamanho");

export const successCreateMovie = () =>
  toast.success("Filme Adicionado Com Sucesso");
export const successUpdateMovie = () =>
  toast.success("Filme Atualizado Com Sucesso");
export const successDeleteMovie = () =>
  toast.success("Filme Eliminado Com Sucesso");
