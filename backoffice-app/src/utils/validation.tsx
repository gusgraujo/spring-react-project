export const createUserValidation = ({
    name,
    discordName,
    discordError,
    setDiscordError,
    setErrorMessage,
    saveUser,
}: any) => {
    if (name === "" || discordName === "") {
        if (name === "" || discordError !== "") {
            setDiscordError("");
            setErrorMessage("The user's name cannot be empty");
        }
        if (discordName === "" || name !== "") {
            setErrorMessage("");
            setDiscordError("The Discord Tag  cannot be empty");
        }
        if (name === "") {
            setErrorMessage("The user's name cannot be empty");
        }
        if (discordName === "") {
            setDiscordError("The Discord Tag  cannot be empty");
        }
        return false;
    }

    saveUser();
    setErrorMessage("");
    setDiscordError("");
};
export const editUserValidation = ({
    name,
    discordName,
    discordError,
    setDiscordError,
    setErrorMessage,
    updateUser,
}: any) => {
    if (name === "" || discordName === "") {
        if (name === "" || discordError !== "") {
            setDiscordError("");
            setErrorMessage("The user's name cannot be empty");
        }
        if (discordName === "" || name !== "") {
            setErrorMessage("");
            setDiscordError("The Discord Tag  cannot be empty");
        }
        if (name === "") {
            setErrorMessage("The user's name cannot be empty");
        }
        if (discordName === "") {
            setDiscordError("The Discord Tag  cannot be empty");
        }
        return false;
    }

    updateUser();
    setErrorMessage("");
    setDiscordError("");

};
