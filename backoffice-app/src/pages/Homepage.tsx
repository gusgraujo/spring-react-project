import HomepageLabels from "../components/HomepageLabels/HomepageLabels";

const Homepage = () => {
  return <HomepageLabels />;
};

export default Homepage;
