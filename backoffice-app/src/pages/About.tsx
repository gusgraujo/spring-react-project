import EditAbout from "../components/about/EditAbout";
import { useState } from "react";

const About: React.FC = () => {
  const [inEdit, setInEdit] = useState<boolean>(false);
  const text: string = "";

  return (
    <>
      <p>About</p>
      <p>{text}</p>
      <button onClick={() => setInEdit(!inEdit)}>edit</button>

      {inEdit && <EditAbout text={text} />}
    </>
  );
};

export default About;
