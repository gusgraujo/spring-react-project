import axios from "axios";
import { useCallback, useEffect, useState } from "react";
import PaginationTest from "../components/PaginationTest";

import { Season } from "../components/Season/Season";
import { seasonsGetAllURL, seasonsSearchURL } from "../utils/apiPaths";

export const Seasons = () => {
  const [search, setSearch] = useState<string>("");
  const [seasons, setSeasons] = useState<any>({});
  const [sort, setSort] = useState<boolean>(true);
  const [refreshContent, setRefreshContent] = useState<number>(0);

  // TEST
  const [currentPage, setCurrentPage] = useState<number>(0);
  const [totalPages, setTotalPages] = useState<number>(0);
  //
  
  const toggleSort = () => {
    setSort(!sort);
  };
  const handleRefresh = () => {
    setRefreshContent((old) => ++old);
  };
  const handleSearch = (searchInputValue: string) => {
    setSearch(searchInputValue);
  };

  const getAllSeasons = useCallback(async () => {
    // Optional parameters as detailed in mockapi's documentation.
    // TODO Pending real API url and parameters
    axios
      .get(
        search.trim().length > 0
          ? seasonsSearchURL(search, 0, 10, "name", sort ? "asc" : "desc")
          : seasonsGetAllURL(0, 10, "name", sort ? "asc" : "desc")
      )
      .then(function ({ data }) {
        setSeasons(data);
        setCurrentPage(data.number + 1);
        setTotalPages(data.totalPages);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [sort, search]);

  useEffect(() => {
    getAllSeasons();
  }, [getAllSeasons, sort, refreshContent, search]);

  return (
    <section>
      {seasons && (
        <>
          <Season
            seasonList={seasons}
            sort={sort}
            toggleSort={toggleSort}
            refreshContent={handleRefresh}
            search={search}
            handleSearch={handleSearch}
          />

          <PaginationTest
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            totalPages={totalPages}
          />
        </>
      )}
    </section>
  );
};
