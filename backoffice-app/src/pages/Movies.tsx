import axios from "axios";
import { SyntheticEvent, useEffect, useState } from "react";
import {
  AiOutlineSortAscending,
  AiOutlineSortDescending,
} from "react-icons/ai";
import { FaSearch } from "react-icons/fa";
import { ToastContainer } from "react-toastify";
import MasterCheckBox from "../components/Checkbox/MasterCheckBox";
import Title from "../components/layout/Title";
import { infoForDeleteModal, infoForEditModal, movieResponseData } from "../components/movies/interfacesMovies";
import CreateMovieModal from "../components/movies/modals/CreateMovieModal";
import DeleteMovieModal from "../components/movies/modals/DeleteMovieModal";
import EditMovieModal from "../components/movies/modals/EditMovieModal";
import MovieTableItem from "../components/movies/MovieTableItem";
import { _Movies } from "../components/movies/_Movies";
import { _MovieTitle } from "../components/movies/_MovieTitle";
import { UserFields } from "../components/User/UserTypes";
import {
  moviesCreateURL,
  moviesDeleteURL,
  moviesGetCurrentSeasonMoviesURL,
  moviesUpdateURL,
  participantsGetAllURL,
} from "../utils/apiPaths";
import {
  successCreateMovie,
  successDeleteMovie,
  successUpdateMovie,
} from "../utils/messages";


const Movies = () => {
  const [search, setSearch] = useState<string>("");
  const [sort, setSort] = useState<boolean>(true);
  const [movieListOriginal, setMovieListOriginal] = useState<Array<movieResponseData>>();
  const [movieList, setMovieList] = useState<Array<movieResponseData>>();
  const [allChecked, setAllChecked] = useState<boolean>(false);

  const [showCreateMovieModal, setShowCreateMovieModal] =
    useState<boolean>(false);
  const [showEditMovieModal, setShowEditMovieModal] = useState<boolean>(false);
  const [showDeleteMovieModal, setShowDeleteMovieModal] =
    useState<boolean>(false);

  const [participantsList, setParticipantsList] = useState<Array<UserFields>>(
    []
  );
  const [movieImdbId, setMovieIMDBId] = useState<string>();
  const [participantId, setParticipantId] = useState<number>(0);

  const [infoForEdit, setInfoForEdit] = useState<infoForEditModal>();
  const [infoForDelete, setInfoForDelete] = useState<infoForDeleteModal>();

  const [refreshContent, setRefreshContent] = useState<number>(0);
  const handleRefresh = () => {
    setRefreshContent((old) => ++old);
  }


  const handleSearch = (searchInputValue: string) => {
    setSearch(searchInputValue);
  };

  const getAllMovies = async () => {
    axios
      .get(moviesGetCurrentSeasonMoviesURL)
      .then(function (response) {
        setMovieListOriginal(response.data);
        setMovieList(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    setSearch("");
    getAllMovies();
    getAllParticipants();
  }, [refreshContent, sort]);

  useEffect(() => {
    setMovieList(movieListOriginal?.filter((movie) => movie.name.includes(search)));
  }, [search]);

  /*
   * Modal Related
   */
  const getAllParticipants = async () => {
    axios
      .get(participantsGetAllURL)
      .then((response) => {
        setParticipantsList(response.data?.content);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  /**
   * Modal preparation/opening and closing
   */
  const toggleAddNewModal = () => {
    if (participantId === 0) setParticipantId(participantsList?.at(0)?.id ?? 0);
    setShowCreateMovieModal(true);
  };
  const toggleEditModal = (infoMovie: infoForEditModal) => {
    setInfoForEdit(infoMovie);
    setShowEditMovieModal(true);
  };
  const toggleDeleteModal = (infoMovie: infoForDeleteModal) => {
    setInfoForDelete(infoMovie);
    setShowDeleteMovieModal(true);
  };
  const closeModals = () => {
    // Close all modals
    setShowCreateMovieModal(false);
    setShowEditMovieModal(false);
    setShowDeleteMovieModal(false);

    // Reset Infos
    setInfoForDelete(undefined);
    setInfoForEdit(undefined);
    setParticipantId(0);
  };

  /**
   * MovieCreate
   */
  const movieCreationPost = (event: SyntheticEvent) => {
    event.preventDefault();
    axios
      .post(moviesCreateURL(movieImdbId, participantId))
      .then((response) => {
        closeModals();
        successCreateMovie();
        handleRefresh();
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /**
   * MovieUpdate
   */
  const movieUpdatePost = (event: SyntheticEvent) => {
    event.preventDefault();
    axios
      .put(moviesUpdateURL(infoForEdit?.id), infoForEdit)
      .then((response) => {
        closeModals();
        successUpdateMovie();
        handleRefresh();
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /**
   * MovieDelete
   */
  const movieDelete = () => {
    axios
      .delete(moviesDeleteURL(infoForDelete?.id ?? 0))
      .then((response) => {
        closeModals();
        successDeleteMovie();
        handleRefresh();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <section>
      <_Movies>
        <div className="pageHeader">
          <section>
            <Title title="Movies" />
          </section>
          <section className="searchAddNewContainer">
            <section className="searchContainer">
              <FaSearch size={15} />
              <input
                type={"text"}
                className="search"
                onChange={(event) => handleSearch(event.target.value)}
                placeholder="Search Movie"
              />
            </section>
            <button
              className="addNewButton"
              type="button"
              onClick={() => {
                toggleAddNewModal();
              }}
            >
              Add New
            </button>
          </section>
        </div>
        <div id="movieTable">
          <section className="seasonTableContainer">
            <section>
              <_MovieTitle>
                <section>
                  <MasterCheckBox
                    allChecked={allChecked}
                    setAllChecked={setAllChecked}
                  />
                </section>
                <section>
                  <button
                    className="sortButton"
                    onClick={() => {
                      /*toggleSort()*/ console.log("sortButton");
                    }}
                  >
                    <article>Movie</article>
                    {sort ? (
                      <AiOutlineSortDescending size={20} />
                    ) : (
                      <AiOutlineSortAscending size={20} />
                    )}
                  </button>
                </section>
                <section>
                  <article>Participant</article>
                </section>
                <section>
                  <article>Status</article>
                </section>
                <section>
                  <article>Actions</article>
                </section>
              </_MovieTitle>
            </section>
            <section className="movieList">
              {movieList &&
                Object.values(movieList).map((movie, index) => (
                  <MovieTableItem
                    movie={movie}
                    key={`movieRowKey${index}`}
                    toggleDeleteModal={toggleDeleteModal}
                    toggleEditModal={toggleEditModal}
                  />
                ))}
            </section>
          </section>
        </div>
        {showCreateMovieModal && (
          <CreateMovieModal
            movieCreationPost={movieCreationPost}
            setMovieIMDBId={setMovieIMDBId}
            setParticipantId={setParticipantId}
            closeModals={closeModals}
            participantId={participantId}
            participantsList={participantsList}
          />
        )}
        {showEditMovieModal && (
          <EditMovieModal
            closeModals={closeModals}
            movieUpdatePost={movieUpdatePost}
            infoForEdit={infoForEdit}
            setInfoForEdit={setInfoForEdit}
            participantsList={participantsList}
          />
        )}
        {showDeleteMovieModal && (
          <DeleteMovieModal
            closeModals={closeModals}
            infoForDelete={infoForDelete}
            movieDelete={movieDelete}
          />
        )}
        <ToastContainer />
      </_Movies>
    </section>
  );
};

export default Movies;
