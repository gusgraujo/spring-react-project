import axios from "axios";
import React, { ChangeEvent, useCallback, useEffect, useState } from "react";
import { FaSearch } from "react-icons/fa";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../App.css";
import photo from "../assets/images/photo.png";
import { searchFilter } from "../utils/filter";
import {
  deleted,
  errorDeleted,
  errorImage,
  errorSuccess,
  errorUpdate,
  success,
  update,
} from "../utils/messages";
//import { URLMOCKAPI, URL_ID } from "../../../utils/mockApi";
import CheckBox from "../components/Checkbox/CheckBox";
import UserImage from "../components/imageUser/userImage";
import Modal from "../components/layout/Modal";
import { Delete } from "../components/layout/Modal/Delete";
import Actions from "../components/layout/Table/Actions";
import { StyledTable } from "../components/layout/Table/StyledTable";
import Title from "../components/layout/Title";
import UserTableTitle from "../components/layout/Title/UserTableTitle";
import PaginationTest from "../components/PaginationTest";
import ToggleSlider from "../components/ToggleSlider/ToggleSlider";
import User from "../components/User/user";
import { UserTypes } from "../components/User/UserTypes";
import {
  participantsCreateURL,
  participantsDeleteURL,
  participantsGetAllURL,
  participantsUpdateURL,
  usersDeleteMultipleURL,
} from "../utils/apiPaths";

const Participants: React.FC = () => {
  const [allChecked, setAllChecked] = useState<boolean>(false);
  const [list, setList] = useState<UserTypes>();
  const [showModal, setShowModal] = useState(false);
  const [spiner, setSpiner] = useState<boolean>(false);
  const [showUserModal, setShowUserModal] = useState(false);
  const [showUser, setShowUser] = useState<string>("");
  const [showSaveButton, setShowSaveButton] = useState<string>("");
  const [showUpdateButton, setShowUpdateButton] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [errorMessage, setErrorMessage] = useState<string>("");
  const [discordError, setDiscordError] = useState<string>("");

  const [discordName, setDiscordName] = useState<string>("");
  const [avatar, setAvatar] = useState<any | null>();
  const [avatarCropped, setCroppedAvatar] = useState<any | null>();
  const [userId, setUserId] = useState<any>(Number);
  const [search, setSearch] = useState<string>("");
  const [checked, setChecked] = useState<Array<any>>([]);
  const [selfChecked, setSelfChecked] = useState<boolean>(allChecked);
  const [showTrashBin, setShowTrashBin] = useState<boolean>(false);
  const [showTitle, setShowTitle] = useState<boolean>(true);
  const [edit, setEdit] = useState<boolean>(false);
  const [statusDraw, setStatusDraw] = useState<boolean>();
  const [refreshContent, setRefreshContent] = useState<number>(0);

  //Search the user name
  const userNameSearch = searchFilter(list?.content, search);

  // TEST
  const [currentPage, setCurrentPage] = useState<number>(0);
  const [totalPages, setTotalPages] = useState<number>(0);
  //

  const handleRefresh = () => {
    setRefreshContent((old) => ++old);
  };

  // display data in put when we are editing
  const setData = (user: any) => {
    setUserId(user.id);
    setName(user.name);
    setAvatar(user?.avatarFileUpload);
    setDiscordName(user.discordName);
  };

  //getAllUsers-fill the user list with data
  const getAllUsers = async () => {
    try {
      const response = await axios.get(participantsGetAllURL);
      setList(response.data);
      setCurrentPage(response.data.number + 1);
      setTotalPages(response.data.totalPages);

      return;
    } catch (error: any) {
      console.log(error);
    }
  };

  const close = () => setShowUserModal(false);

  //collect id and nome of user to be deleted and put in modal
  const handleDelete = (id: Number, name: string) => {
    setShowUser(name);
    setUserId(id);
    setShowModal(true);
  };

  //call user save modal
  const handleSave = () => {
    setShowTitle(true);
    //initialize default picture
    setName("");
    setDiscordName("");
    setDiscordError("");
    setErrorMessage("");
    setAvatar(photo);
    setShowSaveButton("flex");
    setShowUpdateButton("none");
    setShowUserModal(true);
  };

  // call update user  modal
  const handleEdit = (user: Number, allUserInfo: UserTypes) => {
    setShowTitle(false);
    setUserId(user);
    setData(allUserInfo);
    setShowSaveButton("none");
    setShowUpdateButton("flex");
    setShowUserModal(true);
    setName("");
    setDiscordName("");
    setDiscordError("");
    setErrorMessage("");
    setEdit(false);
  };

  //Save new user
  const saveUser = async () => {
    const user: Object = {
      name: name,
      avatar: avatarCropped,
      discordName: discordName,
    };
    setSpiner(true);
    try {
      if (!name) {
        const response = await axios({
          method: "post",
          url: participantsCreateURL,
          data: user,
        });
        if (response.status === 200 || response.status === 201) {
          setSpiner(false);
          success();
          setShowUserModal(false);
          setList(response.data);
          handleRefresh();
          loadTable();
          return response.data;
        } else {
          errorSuccess();
        }
      } else {
        setErrorMessage("The user's name cannot be empty");
      }
    } catch (error: any) {
      if (error.response.status === 413) {
        errorImage();
      }
      errorSuccess();
      console.log(error);
    }
  };

  //updateUser- save new user
  const updateUser = async (id: Number) => {
    const user: Object = {
      name: name,
      avatar: avatarCropped,
      discordName: discordName,
      statusDraw: statusDraw,
    };
    setSpiner(true);
    try {
      const response = await axios({
        method: "put",
        url: participantsUpdateURL(id),
        data: user,
      });
      if (response.status === 200) {
        setSpiner(false);
        update();
        setShowUserModal(false);
        setData(response.data);
        setList(userNameSearch);
        loadTable();
      } else {
        errorUpdate();
      }
    } catch (error: any) {
      if (error.response.status === 413) {
        errorImage();
      }
      errorUpdate();
      console.log(error);
    }
  };

  //updates user visibility for draw in the frontoffice
  const updateStatusDraw = useCallback(
    async (id: Number, statusDraw: boolean) => {
      const user: Object = {
        statusDraw: !statusDraw,
      };
      try {
        const response = await axios({
          method: "put",
          url: participantsUpdateURL(id),
          data: user,
        });
        if (response.status === 200) {
          setSpiner(false);
          loadTable();
        } else {
          errorUpdate();
        }
      } catch (error: any) {
        if (error.response.status === 413) {
          errorImage();
        }
        errorUpdate();
        console.log(error);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [statusDraw]
  );

  const toggleAllSelected = useCallback(() => {
    const user: Object = {
      statusDraw: !statusDraw,
    };
    checked.map(async (str) => {
      try {
        const response = await axios({
          method: "put",
          url: participantsUpdateURL(str),
          data: user,
        });
        if (response.status === 200) {
          setData(response.data);
          loadTable();
        } else {
          errorUpdate();
        }
      } catch (error: any) {
        if (error.response.status === 413) {
          errorImage();
        }
        errorUpdate();
        console.log(error);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [checked, statusDraw]);

  //deleteUser- delete user from list
  const deleteUser = async (id: Number) => {
    console.log(id);
    try {
      const response = await axios.delete(participantsDeleteURL(id));
      if (response.status === 200) {
        deleted();
        setShowModal(false);
        setList(userNameSearch);
        loadTable();
      } else {
        errorDeleted();
      }
    } catch (error: any) {
      errorDeleted();
      console.log(error);
    }
  };

  // Add/Remove all checked items from list
  const handleCheckAll = (event: ChangeEvent<HTMLInputElement>) => {
    let updatedList = [...checked];

    if (event.target.checked) {
      setShowTrashBin(true);
      setSelfChecked(!selfChecked);
      console.log("TTTT", updatedList);
    }
  };

  // Add/Remove checked item from list
  const handleCheck = (event: ChangeEvent<HTMLInputElement>) => {
    let updatedList = [...checked];
    if (event.target.checked) {
      updatedList = [...checked, event.target.value];
      setShowTrashBin(true);
    } else {
      updatedList.splice(checked.indexOf(event.target.value), 1);
      if (updatedList.length === 0) {
        setShowTrashBin(false);
      } else {
        setShowTrashBin(true);
      }
    }

    setChecked(updatedList);
  };

  const deleteAll = async () => {
    const arrOfNum = checked.map((str) => {
      return parseInt(str);
    });
    try {
      const response = await axios.delete(usersDeleteMultipleURL(checked), {
        data: { arrOfNum },
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      deleted();
      setShowTrashBin(false);
      checked.length = 0;
      setList(response.data);
      handleRefresh();
      loadTable();
    } catch (error: any) {
      errorDeleted();
    }
  };

  useEffect(() => {
    getAllUsers();
  }, [search, toggleAllSelected, refreshContent]);

  useEffect(() => {
    setSelfChecked(allChecked);
  }, [allChecked]);

  const loadTable = () => {
    return (
      <StyledTable>
        <div className="pageHeader">
          <section>
            <Title title="Participants" />
          </section>
          <section className="searchAddNewContainer">
            <section className="searchContainer">
              <FaSearch size={15} />
              <input
                type={"text"}
                className="search"
                onChange={(event) => setSearch(event.target.value)}
                placeholder="Search Participant"
              />
            </section>
            <button className="addNewButton" type="button" onClick={handleSave}>
              Add New
            </button>
          </section>
        </div>

        <div
          id="datatable"
          data-mdb-selectable="true"
          data-mdb-multi="true"
          className="userTable"
        >
          <section className="userTableContainer" id="myTable">
            <section>
              <UserTableTitle
                image="Image"
                name="Name"
                discordName="DiscordName"
                actions="Actions"
                setAllChecked={setAllChecked}
                allChecked={allChecked}
                deleteAll={deleteAll}
                toggleAllSelected={toggleAllSelected}
                show={showTrashBin}
              />
            </section>
            <section className="userList">
              {list &&
                Object.values(list.content).map((item, index) => {
                  const {
                    id,
                    name,
                    avatarFileUpload,
                    discordName,
                    statusDraw,
                  }: any = item;
                  return (
                    <section className="userTableRow" key={index}>
                      <section>
                        <CheckBox handleCheck={handleCheck} item={id} />
                      </section>
                      <UserImage avatar={avatarFileUpload} />

                      <section>
                        <article>{name}</article>
                      </section>

                      <section>
                        <article>{discordName}</article>
                      </section>

                      <section>
                        <ToggleSlider
                          statusDraw={statusDraw}
                          updateStatusDraw={updateStatusDraw}
                          userId={id}
                        />
                      </section>

                      <section>
                        <Actions
                          handleEdit={handleEdit}
                          handleDelete={handleDelete}
                          allUserInfo={item}
                          value={id}
                          params={id}
                          name={name}
                        />
                      </section>
                    </section>
                  );
                })}
            </section>
          </section>

          <PaginationTest
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            totalPages={totalPages}
          />

          <Delete
            setShowModal={setShowModal}
            showModal={showModal}
            user={showUser}
            value={userId}
            deleteUser={deleteUser}
          />
          <Modal
            name={name}
            setShowModal={setShowUserModal}
            showModal={showUserModal}
            setNome={setName}
            saveUser={saveUser}
            title="Create User"
            showTitle={showTitle}
            titleUdate={"Update User"}
            valor={userId}
            component={
              <User
                name={name}
                preview={avatar}
                setPreview={setAvatar}
                setNome={setName}
                discordName={discordName}
                setDiscordName={setDiscordName}
                usernameError={errorMessage}
                setErrorMessage={setErrorMessage}
                exit={close}
                showButtonSave={showSaveButton}
                showButtonUpdate={showUpdateButton}
                updateUser={updateUser}
                valor={userId}
                saveUser={saveUser}
                discordError={discordError}
                setDiscordError={setDiscordError}
                setCroppedAvatar={setCroppedAvatar}
                spiner={spiner}
                update={!edit}
              />
            }
          />

          <ToastContainer />
        </div>
      </StyledTable>
    );
  };
  return loadTable();
};

export default Participants;
