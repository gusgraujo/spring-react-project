import { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "./components/Header/Header";
import Login from "./components/login/login";
import Participants from "./pages/Participants";
import Homepage from "./pages/Homepage";
import Movies from "./pages/Movies";
import { Seasons } from "./pages/Seasons";

const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(
    localStorage.getItem("isLoggedIn") !== null
      ? JSON.parse(localStorage.isLoggedIn)
      : false
  );

  useEffect(() => {
    localStorage.setItem("isLoggedIn", String(isLoggedIn));
  }, [isLoggedIn]);

  return (
    <div>
      {isLoggedIn ? (
        <BrowserRouter>
          <Header />
          <Routes>
            <Route path="/" element={<Homepage />} />
            {/* <Route path="/movie" element={<Movie />} /> */}

            <Route path="/movies" element={<Movies />} />
            <Route path="/participants" element={<Participants />} />
            <Route path="/seasons" element={<Seasons />} />
            {/* <Route path="/awards" element={<Awards />} /> */}
          </Routes>
        </BrowserRouter>
      ) : (
        <Login setIsLoggedIn={setIsLoggedIn} />
      )}
    </div>
  );
};
export default App;
