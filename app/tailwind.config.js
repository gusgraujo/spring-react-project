// @type {import('tailwindcss').Config}
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        "regal-blue": "#02061f",
      },
      backgroundImage: {
        "movie-background":
          "linear-gradient(transparent, #02061f), url(./assets/images/poster.png)",
        titanic: "url(./assets/images/titanic.png)",
      },
    },
    fontFamily: {
      SemiBold: ['"Canaro SemiBold"', "sans-serif"],
      SemiBoldItalic: ['"Canaro Semi Bold Italic"', "sans-serif"],
      Black: ['"Canaro Black"', "sans-serif"],
      BlackItalic: ['"Canaro Black Italic"', "sans-serif"],
    },
  },
  plugins: [],
};
