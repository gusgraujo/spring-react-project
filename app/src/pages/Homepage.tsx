import axios from "axios";
import { useEffect, useState } from "react";
import Header from "../components/Header/Header";
import { movieDetails } from "../components/Main/commonInterfaces";
import Main from "../components/Main/Main";
import { moviesGetCurrentMovie } from "../utils/apiPaths";
import styled from "styled-components";

interface homepagePageProps {
  imageUrl: string | undefined;
}

const HomepagePage = styled.div<homepagePageProps>`
  background-image: url(${props => props.imageUrl});
  background-size: cover;
  background-position: center center;
  min-height: 100%;
  position: relative;

  &:before {
    content: "";
    position: absolute;
    background-color: rgba(0, 0, 0, 0.75);
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    z-index: 0;
  }

  > * {
    position: relative;
  }
`;

const Homepage = () => {

  const [chosenMovie, setChosenMovie] = useState<movieDetails>();

  const getChosenMovie = () => {

    axios.get(moviesGetCurrentMovie)
    .then((response) => {
      setChosenMovie(response.data);
    })
    .catch((error) => {
      console.error(error);
    })

  }

  useEffect(() => {
    getChosenMovie();
  }, []);

  return (
    <HomepagePage imageUrl={chosenMovie?.imageURL}>
      <Header />
      <Main chosenMovie={chosenMovie} />
    </HomepagePage>
  );
};

export default Homepage;
