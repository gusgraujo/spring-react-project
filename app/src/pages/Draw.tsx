import React, { useState } from "react";
import styled from "styled-components";
import DrawCard from "../components/draw/DrawCard";
import Header from "../components/Header/Header";

const Drawer = styled.div`
  margin-left: 500px;
  overflow: visible;

  &.maximum5 {
    > :first-child, > :last-child {
      opacity: 33%;
    }
    > :nth-child(2), > :nth-last-child(2) {
      opacity:66%;
    }
  }
  &.minimum7 {
    > :first-child, > :last-child {
      opacity: 25%;
    }
    > :nth-child(2), > :nth-last-child(2) {
      opacity:50%;
    }
    > :nth-child(3), > :nth-last-child(3) {
      opacity:75%;
    }
  }

  > div {
    display: inline-block;
    padding: 100px 20px;
    position: relative;
    perspective: 750px;
    overflow: visible;
  }
  > div > div {
    transform: rotateY(75deg);
    /*background-image: var(--bg);*/
    background-position: center center;
    background-size: cover;
    position: absolute;
    padding: 0 80px;
    height: 100%;
    width: 100%;
    top: 0;
  }
  > div.selected {
    padding: 100px 100px;
    z-index: 10000;
  }
  > div.selected > div {
    transform: rotate(0);
    padding: 0;
  }
  > div.selected ~ div > div {
    transform: rotateY(-90deg);
  }
  .left,
  .left *,
  .right,
  .right *,
  .disappear {
    transition: 0.1s;
  }
  .selected,
  .selected *,
  * {
    transition: all 0.15s, padding 0.1s;
  }
  .disappear,
  .disappear > div {
    padding-left: 0 !important;
    padding-right: 0 !important;
    width: 0 !important;
    transition: all 1s;
  }
`;

const Draw: React.FC = () => {
  // Timer for the roulette
  const [timer, setTimer] = useState<any>(0);
  // Timer to give enough time for the left movie to shrink and appear on the right side
  const [subTimer, setSubTimer] = useState<any>(0);

  const [firstClass, setFirstClass] = useState<string>("left");
  const [selectedClass, setSelectedClass] = useState<string>("selected");
  const [nextClass, setNextClass] = useState<string>("right");

  // TODO: participant information here, instead of movie posters
  const [list, setList] = useState<Array<string | undefined>>([
    `url(https://cdn.europosters.eu/image/750/harry-potter-philosopher-s-stone-i104639.jpg)`,
    `url(https://cdn.europosters.eu/image/750/posters/harry-potter-7-teaser-i123177.jpg)`,
    `url(https://cdn.shopify.com/s/files/1/0057/3728/3618/products/wandavision.mp_240x360_crop_center.progressive.jpg?v=1614371756)`,
    `url(https://cdn.shopify.com/s/files/1/0057/3728/3618/products/c11f66ec0451a36f5aae494b1509b069_bf869227-c34a-449c-83ff-ed093d341dd3_240x360_crop_center.progressive.jpg?v=1573616179)`,
    `url(https://cdn.shopify.com/s/files/1/0037/8008/3782/products/IMG_7259_1024x1024@2x.jpg?v=1640349274)`,
    `url(https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg)`,
    `url(https://images.complex.com/complex/images/c_fill,dpr_auto,f_auto,q_90,w_1400/fl_lossy,pg_1/gdv2pu6io6ekpg5r8mta/back-to-the-future)`,
    `url(https://assets.mubicdn.net/images/notebook/post_images/31857/images-w1400.jpg?1607290863)`,
    `url(https://img.buzzfeed.com/buzzfeed-static/static/2022-04/4/20/asset/0f12255e2129/sub-buzz-817-1649105149-10.jpg)`,
    `url(https://s.yimg.com/ny/api/res/1.2/ZzAHlDHi8a2xdBRRbruaYQ--/YXBwaWQ9aGlnaGxhbmRlcjt3PTY0MDtoPTkyOA--/https://media.zenfs.com/en/homerun/feed_manager_auto_publish_494/d05a3f087fa57f6d41b865d53a42a5f5)`,
    `url(https://images.complex.com/complex/images/c_fill,dpr_auto,f_auto,q_auto,w_1400/fl_lossy,pg_1/wjnhpz3osrai5aningjl/titanic?fimg-client-default)`,
  ]);

  function startRandom() {
    clearTimeout(timer);
    clearTimeout(subTimer);
    roulette(Math.random() * 100 + 10);
  }
  function roulette(rand: number) {
    console.log(rand);
    if (rand < 0) {
      return;
    }
    setTimer(
      setTimeout(() => {
        setSelectedClass("left");
        setNextClass("selected");
        setFirstClass("disappear");
        setSubTimer(
          setTimeout(() => {
            setFirstClass("left");
            setSelectedClass("selected");
            setNextClass("right");
            roulette(--rand);
            const newList: Array<string | undefined> = list;
            const firstElement = newList.shift();
            newList.push(firstElement ?? undefined);
            setList(newList);
          }, 100)
        );
      }, 1)
    );
  }

  return (
    <>
      <Header />
      <Drawer className={list.length < 5 ? "" : ( list.length < 7 ? "maximum5" : "minimum7")}>

        {list.map((item, index, arr) => {
          let pos: string;

          if (index < Math.floor(arr.length / 2)) {
            pos = "left";
          } else if (index === Math.floor(arr.length / 2)) {
            pos = "selected";
          } else {
            pos = "right";
          }

          return (
            <DrawCard
              position={pos}
              image={item}
              first={index === 0}
              firstClass={firstClass}
              nextClass={nextClass}
              selectedClass={selectedClass}
              selected={index === Math.floor(arr.length / 2)}
              nextSelected={index === Math.floor(arr.length / 2) + 1}
            ></DrawCard>
          );
        })}
      </Drawer>
      <button onClick={startRandom}>
        Start
      </button>
    </>
  );
};

export default Draw;
