import axios from "axios";

const URL = "http://localhost:8080/users";

//getAllUsers
export const getAllUsers = () => {
  axios
    .get(URL)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//getUserWithID
export const getUsersWithID = (id: number) => {
  axios
    .get(URL + `/${id}`)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//createUser
export const createUser = (user: Object) => {
  axios
    .post(URL + "/add", user)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//deleteUser
export const deleteUser = (id: number) => {
  axios
    .delete(URL + `/delete/${id}`)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//downloadFile
export const downloadFile = (fileName: string) => {
  axios
    .get(URL + `/downloadFile/${fileName}`)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//getAllUsersSorted
export const getAllUsersSorted = () => {
  axios
    .get(URL + "/getAllSorted")
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//getUsersByName
export const getUsersByName = (name: string) => {
  axios
    .get(URL + `/getName/${name}`)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//getRandomUser
export const getRandomUser = () => {
  axios
    .get(URL + "/randomUser")
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//setAvatar
//TODO: add file
export const setAvatar = (id: number) => {
  axios
    .post(URL + `/setAvatar/${id}`)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//updateUser
export const updateUser = (id: number, user: Object) => {
  axios
    .put(URL + `/update/${id}`, user)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};
