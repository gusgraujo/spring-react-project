import axios from "axios";

const URL = "http://localhost:8080/season";

//createSeason
export const createSeason = (newSeason: Object) => {
  axios
    .put(URL + "/create", newSeason)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//deleteSeason
export const deleteSeason = (id: string) => {
  axios
    .delete(URL + `/delete/${id}`)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//getAllSeasons
export const getAllSeasons = () => {
  axios
    .get(URL + "/getAll")
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//getCurrentSeason
export const getCurrentSeason = () => {
  axios
    .get(URL + "/getCurrent")
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//updateSeason
export const updataSeason = (id: string, newSeason: Object) => {
  axios
    .put(URL + `/update/${id}`, newSeason)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};
