import axios from "axios";

const URL = "http://localhost:8080/movies";

//getMoviesCurrentSeason
export const getMoviesCurrentSeason = () => {
  axios
    .get(URL + "/currentSeason")
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//deleteMovieSelected
export const deleteMovieSelected = (idMovie: string) => {
  axios
    .delete(URL + "/deleteSelected", JSON.parse(idMovie))
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//getMovieById
export const getMovieById = (id: string) => {
  axios
    .get(URL + `/get/${id}`)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//getAllMovies
export const getAllMovies = () => {
  return axios
    .get(URL + "/getAll")
    .then(({ data }) => data)
    .catch(function (error) {
      console.log(error);
    });
};

//getMovieByName
export const getMovieByName = (name: string) => {
  axios
    .get(URL + `/getName/${name}`)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//getMoviesBySeason
export const getMoviesBySeason = (id: number) => {
  axios
    .get(URL + `/getSeason/${id}`)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//getMoviesSeasonsSorted
export const getMoviesSeasonsSorted = (id: number) => {
  axios
    .get(URL + `/getSeasonSorted/${id}`)
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//selectMovie
export const selectMovie = (id: string, userId: number) => {
  axios
    .post(URL + `/select/${id}`, JSON.parse(userId + ""))
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

//setBackgroundMovie
export const setBackgroundMovie = (id: string, url: string) => {
  axios
    .post(URL + `/selectBackground/${id}`, JSON.parse(url))
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
};
