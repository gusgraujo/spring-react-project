import { FC } from "react";
import { Route, Routes } from "react-router-dom";
import About from "./pages/About";
import Awards from "./pages/Awards";
import Draw from "./pages/Draw";
import Homepage from "./pages/Homepage";
import Requests from "./pages/Requests";

const App: FC = () => {
  return (
    <div className="h-screen text-white bg-regal-blue">
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/about" element={<About />} />
        <Route path="/awards" element={<Awards />} />
        <Route path="/draw" element={<Draw />} />
        <Route path="/requests" element={<Requests />} />
      </Routes>
    </div>
  );
};

export default App;
