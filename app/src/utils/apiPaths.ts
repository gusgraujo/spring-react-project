export const URL = `http://localhost:8080/`;

export const seasonsGetAllURL = (page:Number = 0, 
                                size:Number = 10, 
                                sortBy:String = `id`, 
                                sortOrder:String = `asc`
                                ) => URL + `season/getAll?page=${page}&size=${size}&sortBy=${sortBy}&sortOrder=${sortOrder}`;

export const seasonsSearchURL = (search:String = ``,
                                page:Number = 0, 
                                size:Number = 10, 
                                sortBy:String = `id`, 
                                sortOrder:String = `asc`) => URL + `season/${search}?page=${page}&size=${size}&sortBy=${sortBy}&sortOrder=${sortOrder}`;
                                
export const seasonsGetByIdURL = (id:Number) => URL + `season/get/${id}`;
export const seasonsCreateURL = URL + `season/create/`;
export const seasonsUpdateURL = (id:Number) => URL + `season/update/${id}`;
export const seasonsDeleteURL = (id:Number) => URL + `season/delete/${id}`;



export const participantsGetAllURL = URL + `participants/getAll`;
export const participantsGetByNameURL = ( name: string ) => URL + `participants/${name}`;
export const participantsGetAvatarByIdURL = ( id: Number ) => URL + `participants/getAvatar/${id}`;
export const participantsSetAvatarByIdURL = ( id: Number ) => URL + `participants/setAvatar/${id}`;
export const participantsCreateURL = URL + `participants/create`;
export const participantsUpdateURL = ( id: Number ) => URL + `participants/update/${id}`;
export const participantsDeleteURL = ( id: Number ) => URL + `participants/delete/${id}`;
export const participantsDeleteMultipleURL = ( ids: string ) => URL + `participants/deleteMultiple/${ids}`;



export const moviesGetAllURL = URL + `movies/getAll`;
export const moviesGetCurrentSeasonMoviesURL = URL + `movies/currentSeason`;
export const moviesGetCurrentMovie = URL + `movies/getCurrentMovie`;
export const moviesCreateURL = (IMDBId: string = ``, participantId: number) => URL + `movies/select?IMDBId=${IMDBId}&participantId=${participantId}`;
export const moviesUpdateURL = (id: number = 0) => URL + `movies/update/${id}`;
export const moviesDeleteURL = (id: Number) => URL + `movies/delete/${id}`;