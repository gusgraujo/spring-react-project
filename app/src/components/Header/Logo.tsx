import logo from "../../assets/images/colored-logo.png";

type Props = {};

const Logo = (props: Props) => {
  return (
    <div className="font-BlackItalic flex flex-row ml-16 mr-32">
      <img src={logo} alt="logo" className="w-[100px] h-[100px]" />
      <div className="mt-2">
        <p className="text-3xl mb-0 mt-4">BeeWatching</p>
        <p className="font-SemiBold mt-0">Season 1</p>
      </div>
    </div>
  );
};

export default Logo;
