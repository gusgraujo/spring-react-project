import { Link } from "react-router-dom";

type Props = {};

const Navbar = (props: Props) => {
  return (
    <>
      <div className="items-center flex mx-20">
        <Link
          to="/"
          className="font-SemiBold hover:text-[yellow] hover:font-Black"
        >
          HOME
        </Link>
      </div>
      <div className="items-center flex mx-20">
        <Link
          to="/draw"
          className="font-SemiBold hover:text-[yellow] hover:font-Black"
        >
          DRAW
        </Link>
      </div>
      <div className="items-center flex mx-20">
        <Link
          to="/requests"
          className="font-SemiBold hover:text-[yellow] hover:font-Black"
        >
          REQUESTS
        </Link>
      </div>
      <div className="items-center flex mx-20">
        <Link
          to="/awards"
          className="font-SemiBold hover:text-[yellow] hover:font-Black"
        >
          AWARDS
        </Link>
      </div>
      <div className="items-center flex mx-20">
        <Link
          to="/about"
          className="font-SemiBold hover:text-[yellow] hover:font-Black"
        >
          {/* seasons dropdown ? */}
          ABOUT
        </Link>
      </div>
    </>
  );
};

export default Navbar;
