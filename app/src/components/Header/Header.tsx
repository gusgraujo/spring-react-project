import { FC } from "react";
import Logo from "./Logo";
import Navbar from "./Navbar/Navbar";

const Header: FC = () => {
  return (
    <div className="flex flex-row mt-0">
      <Logo />
      <Navbar />
    </div>
  );
};

export default Header;
