import { FC, useEffect, useState } from "react";
import { buildStyles, CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import styled from "styled-components";
import "swiper/css";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from "swiper/react";
import { getAllMovies } from "../../api/Movie";
import "../../assets/css/WatchedMovie.css";
import clock from "../../assets/images/clock.png";

type Props = {};

const TITLE: string = "Previous Movies";

// fake data
const FAKE_DATA_MOVIES = [
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic1",
    rateIMDB: 1,
    rateMeta: 1,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic1",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic2",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic2",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic3",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic3",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic4",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic4",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic5",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic5",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic6",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic6",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic7",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic7",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic8",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic8",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic9",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic9",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic10",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic10",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic11",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic11",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic12",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic12",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic13",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic13",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
  {
    background: "../../assets/images/titanic.png",
    category: "drama",
    createdAt: "2023-02-08T11:47:38.792Z",
    duration: 195,
    guuid: "guuid",
    id: 0,
    idMovie: "idMovie",
    imageURL: "../../assets/images/titanic.png",
    name: "Titanic14",
    rateIMDB: 7.9,
    rateMeta: 7,
    season: [
      {
        createdAt: "2023-02-08T11:47:38.792Z",
        dateEnd: "2023-02-08T11:47:38.792Z",
        dateStart: "2023-02-08T11:47:38.792Z",
        guuid: "guuid",
        id: 1,
        name: "titanic14",
        updatedAt: "2023-02-08T11:47:38.792Z",
      },
    ],
    status: true,
    trailerUrl: "trailer",
    updatedAt: "2023-02-08T11:47:38.792Z",
    year: 2000,
  },
];

// presentational data
const watchedMovies = FAKE_DATA_MOVIES.reverse().map(
  ({ rateIMDB, name, category, year, duration }) => (
    <div className="grid grid-cols-2 grid-rows-6 h-[300px] w-[200px] mx-2 rounded bg-titanic watched-movie group hover:">
      <div className="h-20 w-20 hidden place-self-center group-hover:col-span-2 group-hover:row-span-4 group-hover:block">
        <CircularProgressbar
          value={79}
          text={`${rateIMDB}`}
          styles={buildStyles({
            strokeLinecap: "butt",
          })}
        />
        ;
      </div>
      <div className="ml-4 mt-2 row-start-6 group-hover:row-start-5">
        <p className="uppercase">{name}</p>
        <p className="text-[11px]">{category}</p>
      </div>
      <p className="mt-4 ml-4 hidden group-hover:row-start-6 group-hover:block">
        {year}
      </p>
      <div className="mt-4 ml-4 hidden group-hover:row-start-6 group-hover:col-start-2 group-hover:flex">
        <img src={clock} className="h-4 w-4 mr-1" alt="" />
        <p className="">{duration}</p>
      </div>
    </div>
  )
);

// styling
const StyledBreakpoints = {
  // >= 375px mobile
  375: {
    width: 375,
    slidesPerView: 2,
    spaceBetween: 50,
  },
  // >= 768px tablet
  768: {
    width: 768,
    slidesPerView: 4,
    spaceBetween: 50,
  },
  // >= 1366px pc
  1366: {
    width: 1366,
    slidesPerView: 7,
    spaceBetween: 50,
  },
};

const StyledContainer = styled("div")`
  padding-bottom: 20px;
`;

const Showreel: FC = (props: Props) => {
  const [movies, setMovies] = useState([] as any);

  useEffect(() => {
    getAllMovies().then((newMovies) => {
      // TODO: create array of watchedMovies {} before setting them
      // setMovies(newMovies);
    });

    setMovies(watchedMovies);
  }, []);

  // styling

  return (
    <StyledContainer>
      <h1 className="font-SemiBoldItalic text-3xl mb-4 text-center">{TITLE}</h1>

      {/* FIXME: setting margin is not allowing for the last item to be totally shown */}
      <div className="mx-[15%]">
        <Swiper breakpoints={StyledBreakpoints} scrollbar={{ draggable: true }}>
          {movies.map((movie: any, index: number) => (
            <SwiperSlide key={index}>{movie}</SwiperSlide>
          ))}
        </Swiper>
      </div>
    </StyledContainer>
  );
};

export default Showreel;
