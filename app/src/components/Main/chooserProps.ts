import { participantDetails } from "./commonInterfaces";

export interface chooserProps {
    participant: participantDetails | undefined;
}