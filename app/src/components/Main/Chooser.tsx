import { FC } from "react";
import styled from "styled-components";
import picture from "../../assets/images/photo.png";
import { chooserProps } from "./chooserProps";

const CenteredContainer = styled("div")`
  font-family: Canaro SemiBold;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Chooser = (props: chooserProps) => {
  const {participant} = props;

  return (
    <CenteredContainer>
      <img src={picture  /* participant?.avatarFileUpload */} width="24rem" alt="" />
      <p>{participant?.name}</p>
    </CenteredContainer>
  );
};

export default Chooser;
