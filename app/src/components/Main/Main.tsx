import ChosenMovie from "./ChosenMovie";
import { mainProps } from "./mainProps";
import Showreel from "./Showreel";

const Main = (props: mainProps) => {
  return (
    <>
      <ChosenMovie chosenMovie={props.chosenMovie} />
      <Showreel />
    </>
  );
};

export default Main;
