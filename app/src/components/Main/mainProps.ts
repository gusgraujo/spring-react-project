import { movieDetails } from "./commonInterfaces";

export interface mainProps {
  chosenMovie: movieDetails | undefined;
}