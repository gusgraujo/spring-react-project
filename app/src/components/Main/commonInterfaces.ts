export interface movieDetails {
  name: string;
  idMovie: string;
  status: boolean;
  background: string | undefined;
  imageURL: string | undefined;

  year: number;
  categoryList: string; //Array<string>;
  duration: number;
  rateIMDB: number;
  rateMeta: number;
  trailerUrl: string;
  participants: Array<participantDetails>;
  season: Array<seasonDetails>;
}

export interface participantDetails {
  name: string;
  discordName: string | undefined;
  avatarFileUpload: string | undefined;
}
export interface seasonDetails {
  name: string;
  dateStart: string | undefined;
  dateEnd: string | undefined;
}
