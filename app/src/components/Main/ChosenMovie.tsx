import React from "react";
import clock from "../../assets/images/clock.png";
import imdb from "../../assets/images/imdb.png";
import metacritic from "../../assets/images/metacritic.png";
import play from "../../assets/images/play.png";
import rottenTomatoes from "../../assets/images/rotten-tomatoes.png";
import Chooser from "./Chooser";
import { mainProps } from "./mainProps";

const ChosenMovie = (props: mainProps) => {
  const {chosenMovie} = props;
  return (
    <div className="flex flex-col h-96">
      <p className="font-SemiBoldItalic text-3xl text-center mb-8">
        {chosenMovie?.name}
      </p>

      <div className="flex flex-row justify-center">
        <p className="font-SemiBold mr-4">{chosenMovie?.year}</p>
        <img src={clock} className="w-4 h-4 mb-4 mr-1" alt={clock} />
        <p className="font-SemiBold mr-4">{chosenMovie?.duration}</p>
        <p className="font-SemiBold mr-4">
          {chosenMovie?.categoryList/*.map((cat, index, row) => {
            return cat + (index + 1 === row.length ? "" : "/")
          })*/}
        </p>
      </div>

      <div className="flex flex-row justify-center mb-4">
        <div className="mr-4">
          <div className="h-6 flex justify-center">
            <img src={imdb} className="" alt="" />
          </div>
          <p className="font-SemiBold mt-2 text-center">{chosenMovie?.rateIMDB}/10</p>
        </div>
        {/*<div className="mr-4">
          <div className="h-6 flex justify-center">
            <img src={rottenTomatoes} className="icon" alt="rottenTomates" />
          </div>
          <p className="font-SemiBold mt-2 text-center">74%</p>
        </div>*/}
        <div className="mr-4">
          <div className="h-6 flex justify-center">
            <img src={metacritic} className="icon" alt="metacritic" />
          </div>
          <p className="font-SemiBold mt-2 text-center">{chosenMovie?.rateMeta}%</p>
        </div>
        <div className="mr-4">
          <div className="h-6 flex justify-center">
            <img src={play} className="icon" alt="" />
          </div>
          <p className="font-SemiBold mt-2 text-center">Trailer</p>
        </div>
      </div>

      <Chooser participant={chosenMovie?.participants[0]} />
    </div>
  );
};

export default ChosenMovie;
