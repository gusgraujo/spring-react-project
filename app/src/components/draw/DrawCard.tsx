import React from "react";

interface Props {
  position: string;
  image: string | undefined;
  first: boolean;
  nextSelected: boolean;
  selected: boolean;
  firstClass: string;
  nextClass: string;
  selectedClass: string;
}

const DrawCard = (props: Props) => {
  let classToApply: string;
  if (props.first) classToApply = props.firstClass;
  else if (props.nextSelected) classToApply = props.nextClass;
  else if (props.selected) classToApply = props.selectedClass;
  else classToApply = props.position;
  return (
    <div className={classToApply}>
      <div style={{ backgroundImage: props.image }}></div>
    </div>
  );
};

export default DrawCard;
